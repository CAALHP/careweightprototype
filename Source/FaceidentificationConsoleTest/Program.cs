﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaceIdentificationAdaptor;

namespace FaceidentificationConsoleTest
{
    class Program
    {
        //Small console application that tests faceidentification lib. Program begins by enrolling the users and then trys to identify
        //the user against the template just created.
        static int Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            long totalBytesOfMemoryUsed;



            try
            {

                Console.WriteLine("Trying to enroll from webcam");
                Console.WriteLine("Press anykye to continue");
                Console.Read();

                string id = "peter";
                if (!FaceIdentificationFacade.Instance.ModuleLoaded)
                {
                    Console.WriteLine("Module failed loading");
                    return -1;
                }


                Byte[] template;

                template = FaceIdentificationFacade.Instance.EnrollNewPerson();

                if (template != null)
                    FaceIdentificationFacade.Instance.AddEntry(id, template);
                else
                {
                    Console.WriteLine("Failed creating template");
                    Console.WriteLine("Press anykey to exit");
                    Console.Read();
                    return -1;
                }

                Console.WriteLine( "Template size {0}", template.Length.ToString("N"));

                totalBytesOfMemoryUsed = currentProcess.WorkingSet64;
                Console.WriteLine("Memory Usage: {0}", totalBytesOfMemoryUsed.ToString());
                    

                Console.WriteLine("Press anykey to start identification");
                Console.Read();

                SpeedandMemoryTest(template, 1, id);

                SpeedandMemoryTest(template, 100, id);

                SpeedandMemoryTest(template, 500, id);

                SpeedandMemoryTest(template, 1000, id);

                SpeedandMemoryTest(template, 2000, id);

                SpeedandMemoryTest(template, 5000, id);

                SpeedandMemoryTest(template, 10000, id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                
            }

            Process currentProcess2 = Process.GetCurrentProcess();
            totalBytesOfMemoryUsed = currentProcess2.WorkingSet64;
            Console.WriteLine("Memory Usage: {0}", totalBytesOfMemoryUsed.ToString("N"));

            Console.WriteLine("Press anykey to exit");
            Console.Read();
            return -1;
        }

        private static void SpeedandMemoryTest(byte[] template, int samples, string id)
        {
            Stopwatch stopwatch = new Stopwatch();
            
            long totalBytesOfMemoryUsed;
            

            for (int i = 0; i < samples; i++)
            {
               // Byte[] newid = new byte[]{template};
                Byte[] newid = (Byte[])template.Clone();
                FaceIdentificationFacade.Instance.AddEntry(id + i.ToString(), newid);
            }

            Process currentProcess = Process.GetCurrentProcess(); ;
         

            Console.WriteLine("\nSpeed test {0} records in memory:\nPress any key to start", samples);
            Console.ReadKey();
            
            stopwatch.Start();
            var result = FaceIdentificationFacade.Instance.Identify();
            stopwatch.Stop();

            if (!string.IsNullOrEmpty(result))
                Console.WriteLine("Identification match: {0}", result);
            else
                Console.WriteLine("No match found");

            Console.WriteLine("Time elapsed : {0}", stopwatch.Elapsed.TotalMilliseconds.ToString("N"));
            totalBytesOfMemoryUsed = currentProcess.WorkingSet64;
            Console.WriteLine("Memory Usage: {0}", totalBytesOfMemoryUsed.ToString("N"));

            stopwatch.Reset();

            for (int i = 0; i < samples; i++)
            {
                if (i > 9000)
                {
                    FaceIdentificationFacade.Instance.RemoveEntry(id + i.ToString());
                }
                else
                {
                    FaceIdentificationFacade.Instance.RemoveEntry(id + i.ToString());
                }
                
            }
        }
    }
}
