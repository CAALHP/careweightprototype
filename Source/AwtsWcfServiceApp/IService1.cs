﻿using System.Collections.Generic;
using System.ServiceModel;
using DataTypeLibrary;

namespace AwtsWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        void NewPerson(Person person);

        [OperationContract]
        void NewMeasurement(Measurement measurement);

        [OperationContract]
        List<Measurement> GetMeasurements(string cpr);

        [OperationContract]
        List<Measurement> GetAllMeasurements();

        [OperationContract]
        Person GetPerson(string cpr);

        [OperationContract]
        List<string> GetAllPersonsWithoutIdentification();

        [OperationContract]
        List<Person> GetAllPersonsWithIdentification();
    }
}
