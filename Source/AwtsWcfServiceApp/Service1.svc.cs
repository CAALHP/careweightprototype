﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DB4OHandler;
using DataTypeLibrary;

namespace AwtsWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        // test function
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public void NewPerson(Person person)
        {
            AwtsDbFacade.Instance.NewPerson(person);
        }

        public void NewMeasurement(Measurement measurement)
        {
            AwtsDbFacade.Instance.NewMeasurement(measurement);
        }

        public List<Measurement> GetMeasurements(string cpr)
        {
            return(AwtsDbFacade.Instance.GetMeasurements(cpr));
        }

        public List<Measurement> GetAllMeasurements()
        {
            return(AwtsDbFacade.Instance.GetAllMeasurements());
        }

        public Person GetPerson(string cpr)
        {
            return(AwtsDbFacade.Instance.GetPerson(cpr));
        }

        public List<string> GetAllPersonsWithoutIdentification()
        {
            return(AwtsDbFacade.Instance.GetAllPersonsWithoutIdentification());
        }

        public List<Person> GetAllPersonsWithIdentification()
        {
            return(AwtsDbFacade.Instance.GetAllPersonsWithIdentification());
        }
    }
}
