﻿using System;
using System.Drawing;
using Neurotec.Biometrics;
using Neurotec.Devices;
using Neurotec.IO;
using Neurotec.Licensing;

namespace FaceIdentificationAdaptor
{
    public sealed class FaceIdentificationFacade
    {
        #region private fields

        //Lazy singleton - threadsafe singleton. 
        private static readonly Lazy<FaceIdentificationFacade> lazy = new Lazy<FaceIdentificationFacade>( () => new FaceIdentificationFacade());
        private FaceWrapper faceWrapper;

        public delegate void BroadcastImageEventHandler(object sender, NewImageArgs e);
        public BroadcastImageEventHandler OnBroadCastImage;

        #endregion 

        #region private constructor

        private FaceIdentificationFacade()
        {
            //check if license is installed.
            const int licensePortnumber = 5000;
            if (!NLicense.ObtainComponents("/local", licensePortnumber, Utility.Components))
            {
                ModuleLoaded = false;
                throw new InvalidOperationException("No license Optained");
            }

            //Camera setup.
            NDeviceManager nDevices = new NDeviceManager(NDeviceType.Camera, true, false);
            if (nDevices.Devices.Count == 0)
            {
                ModuleLoaded = false;
                throw new InvalidOperationException("no camera found");
            }

            foreach (var device in nDevices.Devices)
            {
                if (device.DisplayName.ToLower().Contains("logi"))
                {
                    Utility.Camera = (NCamera)device;
                    break;
                }
                    
            }

            if(Utility.Camera == null)
            //uses the build in camera if more then 1 camera is connected.
                Utility.Camera = (NCamera)nDevices.Devices[nDevices.Devices.Count-1];

            Utility.Db = new Database();
            Utility.Extractor = new NLExtractor();
            Utility.Matcher = new NMatcher();

            faceWrapper = new FaceWrapper();

            ModuleLoaded = true;

            faceWrapper.OnBroadCastImage += ForwardOnBroadCastImage;
        }

        private void ForwardOnBroadCastImage(object sender, NewImageArgs newImageArgs)
        {
            if (OnBroadCastImage == null) return;
            OnBroadCastImage(this, newImageArgs);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Returns the instance of the facade.
        /// </summary>
        public static FaceIdentificationFacade Instance {
            get { return lazy.Value; } 
        }

        public bool ModuleLoaded { get; private set; }

        public string LastResult { get; private set; }

        /// <summary>
        /// get or set if identification or enroll is running. 
        /// if running set to false, will stop the process and functions will return null
        /// </summary>
        public bool IsIdentifyingRunning
        {
            get { return faceWrapper.IsIdentifyingRunning; } 
            set { faceWrapper.IsIdentifyingRunning = value; }
        }

        public const string Type = "Face";

        #endregion

        #region public Methods

        /// <summary>
        /// Adds new entry to inmemory database.
        /// </summary>
        /// <param name="id">template identifyer</param>
        /// <param name="template"></param>
        public void AddEntry(string id, byte[] template)
        {
            try
            {
                //converts template into NBuffer from bytearray.
                NBuffer templateToAdd = new NBuffer(template);
                //adds new template to datastructure.
                Utility.Db.AddEntry(id,templateToAdd);
    
            }
            catch (Exception e)
            {
                
                throw e;
            }

        }

        /// <summary>
        /// Adds new entry to inmemory database.
        /// </summary>
        /// <param name="id">template identifyer</param>
        /// <param name="template"></param>
        private void AddEntry(string id, NBuffer template)
        {
            try
            {
                Utility.Db.AddEntry(id, template);
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        /// <summary>
        /// remove a entry from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if element is found and removed</returns>
        public bool RemoveEntry(string id)
        {
           return Utility.Db.RemoveEntry(id);
        }

        /// <summary>
        /// Clears the entry list.
        /// </summary>
        public void ClearEntryList()
        {
            try
            {
                Utility.Db.ClearDB();
            }
            catch (Exception e)
            {
                
                throw e;
            }
        }
        
        /// <summary>
        /// Identifies user using webcam and returns best candidate form DB
        /// </summary>
        /// <returns>Id if succes, else null</returns>
        public string Identify()
        {
            LastResult = null;
            
            LastResult = faceWrapper.Identify(faceWrapper.EnrollFromCamera(NleTemplateSize.Medium));

            return LastResult;
        }

        /// <summary>
        /// Enrolls a new face using webcam, returns template for persisting.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Byte[] EnrollNewPerson()
        {
            var buffer = faceWrapper.EnrollFromCamera(NleTemplateSize.Large);
            return buffer != null ? buffer.ToArray() : null;
        }

        /// <summary>
        /// Enrolls a new face using webcam, returns template for persisting.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool EnrollNewPerson(string id)
        {
            var buffer = faceWrapper.EnrollFromCamera(NleTemplateSize.Large);
            if (buffer == null)
                return false;

            AddEntry(id, buffer);
            return true;
            
        }

        #endregion

    }

}
