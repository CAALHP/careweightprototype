﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Neurotec.IO;

namespace FaceIdentificationAdaptor
{
    internal class Database
    {
        #region private fields

        //private List<Record> _records = new List<Record>();
        private List<Record> _records;
        private string _filename;
        #endregion

        public Database()
        {
            //Check for file name. then read from file, if not possible make new list.
            _filename = Utility.AssemblyDirectory + "\\facialdata.dat";
            try
            {
                if (File.Exists(_filename))
                    ReadFromFile();
                else
                {
                    _records = new List<Record>();
                    //WritetoFile();
                }
            }
            catch (Exception e)
            {
                
                throw e;
            }
               
        }

        #region public properties

        public IEnumerable<Record> Records
        {
            get { return _records; }
        }

        public void ClearDB()
        {
            _records.Clear();
        }

        public void AddEntry(string id, NBuffer template)
        {
            _records.Add(new Record(id,template));

            WritetoFile();
        }

        public bool RemoveEntry(string id)
        {
            try
            {
                var entryToDelete = Records.Single(x => x.Id == id);
                if (_records.Count > 0)
                    _records.Remove(entryToDelete);
                entryToDelete.Template.Dispose();

                WritetoFile();
                
            }
            catch (Exception)
            {
                ReadFromFile();
                return false;
            }

            return true;
        }

        #endregion

        private void ReadFromFile()
        {
            try
            {
                using (Stream stream = File.Open(_filename, FileMode.Open))
                {
                    var bin = new BinaryFormatter();

                    _records = (List<Record>)bin.Deserialize(stream);
                    
                }
            }
            catch (IOException ex)
            {
                throw ex;
            }

        }

        private void WritetoFile()
        {
            try
            {
                using (Stream stream = File.Open(_filename, FileMode.Create))
                {
                    var bin = new BinaryFormatter();
                    bin.Serialize(stream, _records);
                }
            }
            catch (IOException)
            {
            }
            
        }
    }
}
