﻿using System;
using System.Drawing;
using System.IO;
using Neurotec.Biometrics;
using Neurotec.IO;
using Neurotec.Images;

namespace FaceIdentificationAdaptor
{
    internal class FaceWrapper
    {
        public delegate void BroadcastImageEventHandler(object sender, NewImageArgs e);
        public BroadcastImageEventHandler OnBroadCastImage;

        public bool IsIdentifyingRunning { get; set; }

        /// <summary>
        /// Identifys a template against the in memory database.
        /// </summary>
        /// <param name="templateToBeIdentified"></param>
        /// <returns>returns best result, no match return null</returns>
        public string Identify(NBuffer templateToBeIdentified)
        {
            if (templateToBeIdentified == null) return null;

            string result = null;
            int bestscore = 0;

            Utility.Matcher.IdentifyStart(templateToBeIdentified);

            IsIdentifyingRunning = true;

            foreach (var record in Utility.Db.Records)
            {
                var score = Utility.Matcher.IdentifyNext(record.Template);
                if ( score > bestscore)
                {
                    result = record.Id;
                    bestscore = score;
                }
                if (!IsIdentifyingRunning)
                {
                    result = null;
                    break;
                }
            }

            Utility.Matcher.IdentifyEnd();
            IsIdentifyingRunning = false;
            return result;
        }

        /// <summary>
        /// Creates a template using webcam
        /// </summary>
        /// <param name="templateSize">Use large for enrollment to database</param>
        /// <returns></returns>
        public NBuffer EnrollFromCamera(NleTemplateSize templateSize)
        {
            IsIdentifyingRunning = true;
            
            Utility.Extractor.TemplateSize = templateSize;

            bool extractionStarted = false;

            Utility.Camera.StartCapturing();

            try
            {
                while (IsIdentifyingRunning)
                {
                    NImage tmpImage = Utility.Camera.GetFrame();
                    int baseFrameIndex;

                    //if img is null camera is disconnected or not running.
                    if (tmpImage == null)
                    {
                        if (extractionStarted)
                        {
                            NleExtractionStatus extrStatus;
                            NleDetectionDetails details;
                            Utility.Extractor.ExtractEnd(out baseFrameIndex, out details, out extrStatus);
                        }
                        throw new NullReferenceException("Camera is disconnected");
                    }

                    RaiseBroadcastWebcamImage(tmpImage);
                    
                    NGrayscaleImage grayscaleImage = tmpImage.ToGrayscale();
                    if (!extractionStarted)
                    {
                        //checks if face is availeble in picture before starting extraction of template.
                        NleFace face;
                        bool detected = Utility.Extractor.DetectFace(grayscaleImage, out face);
                        if (detected)
                        {
                            extractionStarted = true;
                            Utility.Extractor.ExtractStart();
                        }
                        tmpImage.Dispose();
                    }
                    else
                    {
                        NleDetectionDetails details;
                        NleExtractionStatus status = Utility.Extractor.ExtractNext(grayscaleImage, out details);

                        if (status != NleExtractionStatus.None)
                        {
                            NLTemplate template = Utility.Extractor.ExtractEnd(out baseFrameIndex, out details,
                                                                               out status);
                            if (status == NleExtractionStatus.TemplateCreated)
                            {
                                return template.Save(); 
                            }
                            else
                                //break;
                                throw new InvalidDataException(status.ToString());
                        }
                    }
                    grayscaleImage.Dispose();
                    tmpImage.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (extractionStarted)
                {
                    int baseFrameIndex;
                    NleExtractionStatus status;
                    NleDetectionDetails details;
                    Utility.Extractor.ExtractEnd(out baseFrameIndex, out details, out status);
                }
                if (Utility.Camera != null)
                    Utility.Camera.StopCapturing();

                IsIdentifyingRunning = false;
            }

            return null;
        }

        private void RaiseBroadcastWebcamImage(NImage tmpImage)
        {
            if (OnBroadCastImage == null) return;
            Bitmap imgToSend = tmpImage.ToBitmap();
            OnBroadCastImage(this, new NewImageArgs(imgToSend));
        }
    }
}
