﻿using System;
using System.IO;
using System.Reflection;
using Neurotec.Biometrics;
using Neurotec.Devices;

namespace FaceIdentificationAdaptor
{
    internal static class Utility
    {
        #region private fields

        private static string _components = "Biometrics.FaceExtraction,Devices.Cameras,Biometrics.FaceMatching";

        #endregion

        #region public properties

        public static string Components
        {
            get { return _components; }
            set { _components = value; }
        }
        public static Database Db { get; set; }

        public static NCamera Camera { get; set; }
        public static NLExtractor Extractor { get; set; }
        public static NMatcher Matcher { get; set; }

        #endregion 

        
        #region Methods

        public static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion
    }
}
