﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Events
{
    public class TareRequestEvent: Event
    {
        public string Message { get; set; }
    }
}
