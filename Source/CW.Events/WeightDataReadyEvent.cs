﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Events
{
    public class WeightDataReadyEvent: Event
    {
        public double Value { get; set; }
    }
}
