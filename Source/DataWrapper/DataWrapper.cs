﻿using System;
using System.Collections.Generic;
using DB4OHandler;
using DataTypeLibrary;
using WcfClientlib;

namespace Data
{
    public class DataWrapper
    {
        private static readonly Lazy<DataWrapper> Lazy = new Lazy<DataWrapper>(() => new DataWrapper());

        public static DataWrapper Instance
        {
            get { return Lazy.Value; }
        }


      //  private readonly WcfClient _wcfClient = new WcfClient();

        /// <summary>
        /// Creates a Person object based on the parameters and passes the Person object on to the database and the client.
        /// </summary>
        /// <param name="name">Persons name</param>
        /// <param name="cpr">Persons social security number</param>
        /// <param name="height">Persons height</param>
        /// <param name="idList">List of Identification objects attached to the Person</param>
        /// <param name="center">center name</param>
        public void NewPerson(string name, string cpr, int height, List<Identification> idList, string center)
        {
            var person = new Person(name, cpr, height, idList, center);
            AwtsDbFacade.Instance.NewPerson(person);
           // _wcfClient.NewPerson(person);
        }

        /// <summary>
        /// Creates a Measurement object based on the parameters and passes the Person object on to the database and the client.
        /// </summary>
        /// <param name="type">Type of Measurement</param>
        /// <param name="value">Value of Measurement</param>
        /// <param name="pers">Person object attached to the Measurement</param>
        /// <param name="quality">Quality factor of Measurement</param>
        public void NewMeasurement(string type, double value, Person pers = null, string quality = null)
        {

            var measurement = new Measurement(type, value, pers, quality);
            AwtsDbFacade.Instance.NewMeasurement(measurement);

         //   if(pers != null)
         //     _wcfClient.NewMeasurement(measurement);

           
        }

        /// <summary>
        /// Returns a person from the data  base if found.
        /// </summary>
        /// <param name="cpr">Persons social security number</param>
        /// <returns>Person object if found, else null</returns>
        public Person GetPerson(string cpr)
        {
            return AwtsDbFacade.Instance.GetPerson(cpr);
        }

        /// <summary>
        /// Gets all personnames in the database
        /// </summary>
        /// <returns>List of strings with personnames</returns>
        public List<string> GetAllPersonsWithoutIdentification()
        {
            return AwtsDbFacade.Instance.GetAllPersonsWithoutIdentification();
        }

        /// <summary>
        /// Get all person objects in the database, with identification data.
        /// </summary>
        /// <returns>List with all Person objects in the database.</returns>
        public List<Person> GetAllPersonsWithIdentification()
        {
            return AwtsDbFacade.Instance.GetAllPersonsWithIdentification();
        }
    }
}
