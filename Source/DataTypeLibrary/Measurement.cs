﻿using System;
using System.Runtime.Serialization;

namespace DataTypeLibrary
{
    [Serializable]
    [DataContract]
    public class Measurement
    {
        #region Constructor

        public Measurement()
        {}

        public Measurement(string type, double value, Person person = null, string quality = null)
        {
            Type = type;
            Value = value;
            Date = DateTime.Now;
            Person = person;
            Quality = quality;
        }

        #endregion

        #region Public properties

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public double Value { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public Person Person { get; set; }

        [DataMember]
        public string Quality { get; set; }

        #endregion
    }
}
