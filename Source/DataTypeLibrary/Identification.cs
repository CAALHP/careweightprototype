﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataTypeLibrary
{
    [Serializable]
    [DataContract]
    public class Identification
    {
        #region Constructor
        //todo remove ID and make it handled by awtsdbfacade

        public Identification()
        {}

        public Identification(byte[] value, int id, string type)
        {
            Value = value;
            Id = id;
            Type = type;
        }

        #endregion

        #region Public properties
        
        [DataMember]
        public byte[] Value { get; set; }

        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Type of Identification, e.g. Facial Recognition, RFID and such.
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        #endregion
    }
}
