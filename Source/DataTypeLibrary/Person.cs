﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DataTypeLibrary
{
    [Serializable]
    [DataContract]
    public class Person
    {
        private string _id;

        #region Constructor

        public Person()
        {}

        public Person(string name, string cpr, int height, List<Identification> idList, string center)
        {
            Name = name;
            Cpr = cpr;
            Height = height;
            IdList = idList;
            TimeofCreation = DateTime.Now;
            Center = center;
            Id = Center + " " + TimeofCreation.ToString();
        }

        #endregion

        #region Public properties

        [DataMember]
        public string Name { get; protected set; }

        [DataMember]
        public string Cpr { get; protected set; }

        [DataMember]
        public int Height { get; protected set; }

        [DataMember]
        public List<Identification> IdList { get; protected set; }

        [DataMember]
        public DateTime TimeofCreation { get; protected set; }

        [DataMember]
        public string Id
        {
            get { return _id; }
            protected set { _id = value; }
        }

        [DataMember]
        public string Center { get; protected set; }
        #endregion

        public void AddIdentification(Identification i)
        {
            IdList.Add(i);
        }
    }
}
