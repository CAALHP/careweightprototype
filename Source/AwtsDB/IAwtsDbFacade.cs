﻿using System.Collections.Generic;
using System.ServiceModel;
using DataTypeLibrary;

namespace DB4OHandler
{
    [ServiceContract]
    public interface IAwtsDbFacade
    {
        [OperationContract]
        void NewPerson(Person person);

        [OperationContract]
        void NewMeasurement(Measurement measurement);

        [OperationContract]
        List<Measurement> GetMeasurements(string id);

        [OperationContract]
        List<Measurement> GetAllMeasurements();

        [OperationContract]
        Person GetPerson(string cpr);

        [OperationContract]
        List<string> GetAllPersonsWithoutIdentification();

        [OperationContract]
        List<Person> GetAllPersonsWithIdentification();

        [OperationContract]
        List<Person> GetAllPersonsAtCenter(string center);

        [OperationContract]
        List<Measurement> GetMeasurementsAndroid(string cpr);
    }
}