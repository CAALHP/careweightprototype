﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using DataTypeLibrary;
using Db4objects.Db4o;
using Db4objects.Db4o.Linq;

namespace DB4OHandler
{
    public class AwtsDbFacade : IAwtsDbFacade
    {
        private static readonly Lazy<AwtsDbFacade> lazy = new Lazy<AwtsDbFacade>(() => new AwtsDbFacade());
       
        // path to db4o file
        private static string _filePath;

        public AwtsDbFacade()
        {
            //checks if bin folder exsists in current domain of the application, if not creates the folder, and stores yap in this folder.
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "bin"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "bin");
            }

            _filePath = AppDomain.CurrentDomain.BaseDirectory + "bin\\Awtsdb.yap";

            using (Db4oFactory.OpenFile(_filePath))
            {
            }
        }

        public static AwtsDbFacade Instance
        {
            get { return lazy.Value; }
        }

        #region Public methods

        /// <summary>
        /// store person in database
        /// </summary>
        /// <param name="person">person object</param>
        public void NewPerson(Person person)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                db.Store(person);
            }
            
        }

        /// <summary>
        /// store measurement in database
        /// </summary>
        /// <param name="measurement">Measurements object</param>
        public void NewMeasurement(Measurement measurement)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                if (measurement.Person != null)
                {
                    //retrives the person from the db to add it as refrence to measurement, if this is not done a new person is added.
                    //if no person found adds new person.
                    IEnumerable<Person> result = from Person p in db
                                                 where p.Id == measurement.Person.Id
                                                 select p;
                    if (result.Any())
                        measurement.Person = result.First();
                }

                db.Store(measurement);
            }
        }

        /// <summary>
        /// Get a Measurements List with all Measurements matching the given cpr from the database
        /// </summary>
        /// <param name="id">Persons cpr as a string</param>
        /// <returns>list with Measurements objects matching the given cpr number</returns>
        public List<Measurement> GetMeasurements(string id)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (      from Measurement m in db
                                    where m.Person != null && m.Person.Id == id 
                                    select m).ToList();
                
                return result;
            }
        }

        /// <summary>
        /// Get all Measurements object from the database
        /// </summary>
        /// <returns>list with all Measurements objects in the database </returns>
        public List<Measurement> GetAllMeasurements()
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (from Measurement m in db
                              select m).ToList();

                return result;
            }
        }

        /// <summary>
        /// Get a person object from the database
        /// </summary>
        /// <param name="cpr"></param>
        /// <returns>Person object matching cpr</returns>
        public Person GetPerson(string cpr)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = GetPerson(cpr, db);

                return result;
            }
        }

        private static Person GetPerson(string cpr, IObjectContainer db)
        {
            var persons = from Person p in db
                         select p;

            var result = from p in persons
                         where p.Cpr == cpr
                         select p;

            return result.FirstOrDefault();
        }


        /// <summary>
        /// Gets all personnames in the database
        /// </summary>
        /// <returns>List of strings with personnames</returns>
        public List<string> GetAllPersonsWithoutIdentification()
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (from Person p in db
                                       where p != null
                                       select p.Name).ToList();

                return result;
            }     
        }

        /// <summary>
        /// Get all person objects in the database, with identification data.
        /// </summary>
        /// <returns>List with all Person objects in the database.</returns>
        [OperationContract]
        public List<Person> GetAllPersonsWithIdentification()
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (from Person p in db
                                       where p != null
                                       select p).ToList();
                return result;
            }
        }


        /// <summary>
        /// Get all persons objects that belongs to a specific center.
        /// </summary>
        /// <param name="center">list of person objects</param>
        /// <returns></returns>
        public List<Person> GetAllPersonsAtCenter(string center)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (from Person p in db
                              where p.Center == center
                              select p).ToList();
                return result;
            }
        }

        // for Android application
        public List<Measurement> GetMeasurementsAndroid(string cpr)
        {
            using (var db = Db4oFactory.OpenFile(_filePath))
            {
                var result = (from Measurement m in db
                              where m.Person != null && m.Person.Cpr == cpr
                              select m).ToList();

                return result;
            }
        }

        #endregion
        //Remarks: this should maybe be a singleton. 1 db for entire project.
    }
}