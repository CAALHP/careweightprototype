﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Threading;
using DataTypeLibrary;
using WcfClientlib.AwtsWebServiceRef;
using Measurement = DataTypeLibrary.Measurement;

//using AwtsWcfClient.AwtsServiceReference;


namespace WcfClientlib
{
    public class WcfClient
    {

        #region fields

        private readonly string _endpointadress;
        private readonly string _path;

        private List<Person> _personsNotSend;
        private List<Measurement> _measurementsNotSend;
        private bool _isThreadrunning;

        #endregion
        
        #region constructor

        public WcfClient()
        {
            // On succesfull builds, remember to move the Awts.WcfClient.dll.Config file to the project using the client.
            Configuration config = null;

            _personsNotSend = new List<Person>();
            _measurementsNotSend = new List<Measurement>();

            //gets the path for the serialized file.
            _path = Assembly.GetEntryAssembly().GetName().Name + ".bin";

            _endpointadress = null;
            // Gets the servicepath from the Configuration AppSettings.
            string exeConfigPath = GetType().Assembly.Location;
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
            }
            catch (Exception)
            {

            }

            if (config != null)
            {
                _endpointadress = GetAppSetting(config, "endpoint");

            }

            DeserializeObjects();

        }

        #endregion
        
        #region public methods
        
        /// <summary>
        /// Adds the Person object to the list and pushes it to the database.
        /// </summary>
        /// <param name="pers">Person object to be send</param>
        public void NewPerson(Person pers)
        {
            lock (_personsNotSend)
            {
                _personsNotSend.Add(pers);
            }

            StartSend();
        }

        /// <summary>
        /// Adds the Measurement object to the list and pushes it to the database.
        /// </summary>
        /// <param name="meas">Measurement object to be send</param>
        public void NewMeasurement(Measurement meas)
        {
            lock (_measurementsNotSend)
            {
                _measurementsNotSend.Add(meas);
            }

            StartSend();
        }

        public List<Measurement> GetMeasurements(string id, string pass)
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                return (client.GetMeasurements(id, pass));
            }

        }

        public List<string> GetAllPersonsWithoutIdentification(string pass)
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                return (client.GetAllPersonsWithoutIdentification(pass));
            }

        }

        public List<Person> GetAllPersonsWithIdentification(string pass)
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                return (client.GetAllPersonsWithIdentification(pass));
            }

        }

        public bool Login(string centerid, string pass)
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                return client.Login(centerid, pass);
            }
        }

        public List<Person> GetAllPersonsAtCenter(string center, string pass)
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                return (client.GetAllPersonsAtCenter(center, pass));
            }

        }

        #endregion

        #region private methods

        /// <summary>
        /// Starts a new thread for sending data to webservice. 
        /// </summary>
        private void StartSend()
        {

            if (!_isThreadrunning)
            {
                _isThreadrunning = true;
                Thread thread = new Thread(Send);
                thread.Start();
            }

        }

        /// <summary>
        /// Checks is the lists with Person objects and the list with Measurement objects are empty, if not it pushes them to the database.
        /// </summary>
        private void Send()
        {
            using (var client = new AwtsWebServiceClient(new BasicHttpBinding(), new EndpointAddress(_endpointadress)))
            {
                //while person and measurement lists contails elements thread is kept alive, sending one at a time.
                while (_isThreadrunning)
                {
                    if (_personsNotSend.Count > 0)
                    {
                        Person p = null;
                        
                        lock (_personsNotSend)
                        {
                            if (_personsNotSend.Count > 0)
                            {
                                p = _personsNotSend[0];
                            }
                        }

                        if (p == null) break;

                        try
                        {
                            //clears id list for data to send less data to server. will couse fail to send if not
                            p.IdList.Clear();
                            //sends newperson to webserver, removes it form list if sent. 
                            client.NewPerson(p);
                            lock (_personsNotSend)
                            {
                                _personsNotSend.Remove(p);
                            }
                        }
                        catch (Exception)
                        {
                            break;
                        }

                    }
                    else if (_measurementsNotSend.Count > 0)
                    {
                        Measurement m = null;
                       
                        lock (_measurementsNotSend)
                        {
                            if (_measurementsNotSend.Count > 0)
                            {
                                m = _measurementsNotSend[0];
                            }
                        }

                        if (m == null) break;

                        try
                        {
                            //clears id list for data to send less data to server. will couse fail to send if not
                            m.Person.IdList.Clear();
                            //sends measurement to webserver, removes it form list if sent. 
                            client.NewMeasurement(m);
                            lock (_measurementsNotSend)
                            {
                                _measurementsNotSend.Remove(m);
                            }
                        }
                        catch (Exception)
                        {
                            break;
                        }

                    }
                    else
                    {
                        //lists are empty, and file now contains already sent data. 
                        File.Delete(_path);
                        break;
                    }
                }

            }

            //Serializes all objects not send, either couse send failed, or added after while exit.
            if (_personsNotSend.Count > 0 || _measurementsNotSend.Count > 0)
            {
                SerializeObjects();
            }

            //Sets isthreadrunning to false, so new thread can be started.
            _isThreadrunning = false;
        }

        /// <summary>
        /// Returns value from appsettings in configfile
        /// </summary>
        /// <param name="config">path to configfile</param>
        /// <param name="key">key name</param>
        /// <returns></returns>
        private string GetAppSetting(Configuration config, string key)
        {

            KeyValueConfigurationElement element = config.AppSettings.Settings[key];
            if (element != null)
            {
                string value = element.Value;
                if (!string.IsNullOrEmpty(value))
                    return value;
            }
            return string.Empty;
        }

        /// <summary>
        /// Private method to serialize Person and Measurement objects.
        /// </summary>
        private void SerializeObjects()
        {
            //Serialized the 2 lists to file.
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(_path, FileMode.Create, FileAccess.Write, FileShare.None);
            try
            {

                lock (_personsNotSend)
                {
                    formatter.Serialize(stream, _personsNotSend);
                }
                lock (_measurementsNotSend)
                {
                    formatter.Serialize(stream, _measurementsNotSend);
                }

            }
            catch (Exception)
            {


            }
            finally
            {
                stream.Close();
            }
        }

        /// <summary>
        /// Private method to deserialize Person and Measurement objects.
        /// </summary>
        private void DeserializeObjects()
        {
            //if no objects serialized file does not exsists
            if (!File.Exists(_path)) return;

            //opens and loads serialized lists into memory.
            using (FileStream fs = File.OpenRead(_path))
            {
                var bf = new BinaryFormatter();
                try
                {
                  lock (_personsNotSend)
                    {
                        _personsNotSend = (List<Person>) bf.Deserialize(fs);
                    }
                    lock (_measurementsNotSend)
                    {
                        _measurementsNotSend = (List<Measurement>) bf.Deserialize(fs);
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        #endregion

    }
}
