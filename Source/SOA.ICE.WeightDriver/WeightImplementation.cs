﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CW.Events;
using WeightInterface;

namespace SOA.ICE.WeightDriver
{
    public class WeightImplementation : IDeviceDriverCAALHPContract
    {
        private int _processId;
        public IDeviceDriverHostCAALHPContract Host { get; set; }
        private Weight _weight;
        //private Timer _connectionTimer;

        public WeightImplementation()
        {
            _weight = new Weight();

            _weight.OnTareCompleted += ForwardWeightOnTareCompleted;
            _weight.OnUserOffWeight += ForwardOnUserOffWeight;
            _weight.OnUserOnWeight += _weight_OnUserOnWeight;
            _weight.OnWeightDataReady += _weight_OnWeightDataReady;
            _weight.OnWeightInit += _weight_OnWeightInit;
            _weight.OnConnectionError += _weight_OnConnectionError;

            //_connectionTimer = new Timer(10000);
            //_connectionTimer.Elapsed += ConnectionTimerElapsed;

            //_connectionTimer.Start();
        }

        void _weight_OnConnectionError(object sender, EventArgs e)
        {
            var tareEvent = new ConnectionErrorEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, tareEvent, "CW.Events");
            Host.Host.ReportEvent(serializedList);
        }

        void _weight_OnWeightInit(object sender, EventArgs e)
        {
            var inivEvent = new WeightInitEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, inivEvent, "CW.Events");
            Host.Host.ReportEvent(serializedList);
        }

        private void _weight_OnWeightDataReady(object sender, WeightEventArgs e)
        {
            var dataEvent = new WeightDataReadyEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    Value = e.Buffer
                };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, dataEvent, "CW.Events");
            Host.Host.ReportEvent(serializedList);
        }

        private void _weight_OnUserOnWeight(object sender, EventArgs e)
        {
            var userOnWeightEvent = new UserOnWeightEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, userOnWeightEvent, "CW.Events");
            Host.Host.ReportEvent(serializedList);
        }

        private void ForwardOnUserOffWeight(object sender, EventArgs e)
        {
            var userOffWeightEvent = new UserOffWeightEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, userOffWeightEvent, "CW.Events");
            Host.Host.ReportEvent(serializedList);
        }

        private void ForwardWeightOnTareCompleted(object sender, TareEventArgs e)
        {
            if (e.Buffer.ToLower().Contains("ok"))
            {
                var tareEvent = new TareResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

                var serializedList = EventHelper.CreateEvent(SerializationType.Json, tareEvent, "CW.Events");
                Host.Host.ReportEvent(serializedList);
                
            }
            else
            {
                var tareEvent = new TareErrorResponsEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

                var serializedList = EventHelper.CreateEvent(SerializationType.Json, tareEvent, "CW.Events");
                Host.Host.ReportEvent(serializedList);
            }
        }

        public void HandleEvent(WeighingRequestEvent obj)
        {
            _weight.ManualWeighing();
        }

        public void HandleEvent(TareRequestEvent obj)
        {
            _weight.ManualTare();
        }

        //private void ConnectionTimerElapsed(object sender, ElapsedEventArgs e)
        //{
        //    if (_weight.IsOpen()) return;

        //    Console.WriteLine("No connection, trying to reconnect.");
        //    ReportNoConnection();
        //    _weight.TryOpen();
        //}

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        public string GetName()
        {
            return "WeightDriver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            Host = hostObj;
            _processId = processId;

            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(WeighingRequestEvent), "CW.Events"), _processId);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TareRequestEvent), "CW.Events"), _processId);

            Console.WriteLine("WeightDriver Initialized");
        }

        //this does not work
        public void Start()
        {
            
        }

        public void Stop()
        {
            Environment.ExitCode = 0;
            Environment.Exit(Environment.ExitCode);
        }
    }
}
