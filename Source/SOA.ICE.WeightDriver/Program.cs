﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.SOA.ICE.ClientAdapters;

namespace SOA.ICE.WeightDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            //we assume the host is running on localhost:
            const string endpoint = "localhost";
            try
            {
                //creating the ServiceAdapter - which also handles connection to the endpoint. 
                //The second parameter is a reference to the implementation of an IServiceCAALHPContract.
                //Debugger.Launch();
                
                //var logImp = new LogImplementation();
                var weightImp = new WeightImplementation();
                var adapter = new DeviceDriverAdapter(endpoint, weightImp);
                Console.WriteLine("WeightImplementation running");
                Console.WriteLine("press <enter> to exit...");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
