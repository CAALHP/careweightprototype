﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB4OHandler;
using DataTypeLibrary;

namespace DataToCSV
{
    //public class Measurement
    //{
    //    public Measurement(string _type, double _value, int _id, DateTime datetime, Person person = null)
    //    {
    //        Type = _type;
    //        Value = _value;
    //        Id = _id;
    //        Date = datetime;
    //        if (person != null)
    //        {
    //            Name = person.Name;
    //        }
    //        else
    //        {
    //            Name = "";
    //        }
    //    }

    //    public string Type { get; private set; }

    //    public double Value { get; private set; }

    //    public int Id { get; private set; }

    //    public DateTime Date { get; private set; }

    //    public string Name { get; private set; }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            const string filePath = @"C:\temp\measurements.csv";

            //if (File.Exists(filePath))
            //{
            //    File.Delete(filePath);
            //}   

            var idList = new List<Identification> { new Identification(new byte[1], 1, "Facial recognition") };

            // If no data is in the database, these functions can add some.
           // Person p = new Person("Christian", "0807892711", 100, idList,"iha");
           //// AwtsDbFacade.Instance.NewPerson(p);

           // Measurement mea = new Measurement("Weight", 75.8,p);
           // AwtsDbFacade.Instance.NewMeasurement(mea);
           // AwtsDbFacade.Instance.NewMeasurement(mea);
           // AwtsDbFacade.Instance.NewMeasurement(mea);
           // AwtsDbFacade.Instance.NewMeasurement(mea);
           // AwtsDbFacade.Instance.NewMeasurement(mea);
           // AwtsDbFacade.Instance.NewMeasurement(mea);

            //AwtsDbFacade.Instance.NewPerson("Helle", "12345678", 200, idList);
            //AwtsDbFacade.Instance.NewMeasurement("Weight", 200, "12345678");
            //AwtsDbFacade.Instance.NewMeasurement("Weight", 200);

            var measurementsList = AwtsDbFacade.Instance.GetAllMeasurements();

            var personlist = AwtsDbFacade.Instance.GetAllPersonsWithIdentification();
            Console.WriteLine(personlist.Count);

            //var csvList = new List<Measurement>();
            var csvList = new List<Simplemeasurement>();
            foreach (var m in measurementsList)
            {
                var measure = new Simplemeasurement
                    {
                        Name = m.Person.Name,
                        Center = m.Person.Center,
                        Type = m.Type,
                        Value = m.Value,
                        Quality = m.Quality,
                        Date = m.Date
                    };
                // csvList.Add(new Measurement(m.Type, m.Value, m.Person, m.Quality));
                csvList.Add(measure);
            }
            //var csv = measurementsList.GetCSV();
            var csv = csvList.GetCSV();

            File.WriteAllText(filePath, csv);

        }
    }

    public class Simplemeasurement
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Center { get; set; }
        public string Type { get; set; }
        public double Value { get; set; }
        public string Quality { get; set; }

    }
}
