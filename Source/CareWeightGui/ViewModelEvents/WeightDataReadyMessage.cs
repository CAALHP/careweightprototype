﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareWeight.ViewModelEvents
{
    public class WeightDataReadyMessage: BaseMessage
    {
        public double Value { get; set; }
    }
}
