﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CareWeight.ViewModelEvents
{
    public class IdentificationDoneMessage : BaseMessage
    {
        public UserProfile User { get; set; }
    }
}
