﻿using Caliburn.Micro;

namespace CareWeight.ViewModelEvents
{
    public class ViewModelEventAggregator
    {
        static EventAggregator eventAggregator;

        public static EventAggregator EventAggregator
        {
            get { return eventAggregator ?? (eventAggregator = new EventAggregator()); }
        }
        
    }

}
