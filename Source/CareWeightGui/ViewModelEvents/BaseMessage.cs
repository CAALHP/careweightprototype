﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareWeight.ViewModelEvents
{
    public abstract class BaseMessage
    {
        public Type Sender { get; set; }
    }
}
