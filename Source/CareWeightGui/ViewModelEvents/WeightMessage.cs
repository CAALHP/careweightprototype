﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CareWeight.ViewModelEvents
{
    public class WeightMessage: BaseMessage
    {
        public double Value { get; set; }
        public UserProfile User { get; set; }
    }
}
