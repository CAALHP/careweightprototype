﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Infralution.Localization.Wpf;

namespace CareWeight.Tools
{
    public static class LanguageManager
    {

        #region fields

        private static readonly List<CultureInfo> cultList = new List<CultureInfo>();

        #endregion

        #region constructor

        /// <summary>
        /// Initiates lists with all cultures availeble to the program.
        /// Retrives default cultureinfo from app.config appsettings key=language
        /// Default value if not set, en-US.
        /// </summary>
        static LanguageManager()
        {
            try
            {
                DefaultCulture = ConfigurationManager.AppSettings["language"];
            }
            catch (Exception)
            {
                DefaultCulture = "en-US";
            }



            CultList.Add(new CultureInfo(DefaultCulture));
            //_cultList.Add(CultureInfo.InvariantCulture);

            CultureInfo[] tmp = CultureInfo.GetCultures(CultureTypes.AllCultures);
            foreach (var ci in tmp)
            {
                if (File.Exists(GetSatelliteAssemblyName(ci)))
                {
                    try
                    {
                        if (Assembly.GetExecutingAssembly().GetSatelliteAssembly(ci) != null)
                        {
                            CultList.Add(ci);
                        }
                    }
                    catch (Exception)
                    {
                        // todo: something?
                    }
                }
            }
        }

        #endregion
        
        #region properties

        public static List<CultureInfo> CultList
        {
            get { return cultList; }
        }

        /// <summary>
        /// Sets the defaultcultureinfo used by the class. 
        /// use the native format. ie. "en-US" or "fr-FR"
        /// Default value = "en-US"
        /// </summary>
        private static string DefaultCulture { get; set; }

        #endregion
        
        #region Methods

        /// <summary>
        /// Sets the default initial culture.
        /// </summary>
        public static void Init()
        {
            // set default culture
            // only for this threadjavascript:void(0)
            // from CultureInfo.CurrentCulture Property 
            // Note that if you set a specific culture that is different from the system-installed culture or the user's preferred culture, and your application starts multiple threads, the current culture of those threads will be the culture returned by the GetUserDefaultLocaleName function.
            CultureManager.UICulture = CultureInfo.InvariantCulture;
            CultureManager.SynchronizeThreadCulture = false; // to affect only ui (not parse/write)
        }

        /// <summary>
        /// Constructs and returns a path of file for cultureinfo ci.
        /// </summary>
        /// <param name="ci"></param>
        /// <returns></returns>
        private static string GetSatelliteAssemblyName(CultureInfo ci)
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ci.Name + "\\" +
                   Assembly.GetExecutingAssembly().GetName().Name + ".resources.dll";
        }

        /// <summary>
        /// Changes the the settings to the selected, raises event to update all UI controls. 
        /// </summary>
        public static bool ChangeLanguage(int selectedlng)
        {
            try
            {
                CultureInfo newCulture = CultList[selectedlng];
                if (newCulture.Name == DefaultCulture)
                {
                    newCulture = CultureInfo.InvariantCulture;
                }
                if (CultureInfo.CurrentUICulture != newCulture)
                {
                    CultureManager.UICulture = newCulture;
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        public static int GetIndex(string language)
        {
            if (language == null)
            {
                language = DefaultCulture;
            }
            //language = language == "en-US" ? "" : language;
            return CultList.IndexOf(new CultureInfo(language));
        }

        public static string GetCultureString(int index)
        {
            string language = CultList.ElementAt(index).Name;
            return language;
            //return language == "" ? "en-US" : language;
        }

        public static int GetIndexWithoutInvariant(string language)
        {
            return GetIndex(language == "" ? DefaultCulture : language);
        }

        #endregion

    }
}
