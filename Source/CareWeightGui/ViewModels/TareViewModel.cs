﻿using System.ComponentModel.Composition;
using CareWeight.ViewModelEvents;
using Caliburn.Micro;
using CareWeight.Resource;

namespace CareWeight.ViewModels
{
    [Export(typeof(TareViewModel))]
    public class TareViewModel : BaseViewModel, IHandle<ViewModelMessage>
    {
        private string _tareInfoText;
        private bool _tareError;


        #region constructor

        public TareViewModel()
        {
            Name = "Tare";

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            StatusMessage = Resource.Resource.TareViewModel_TareViewModel_Venter_på_tarering_kan_begynde_;
            
            TareInfoText = Resource.Resource.TareViewModel_TareViewModel_;

        }

        #endregion

        public string TareInfoText
        {
            get { return _tareInfoText; }
            set
            {
                if (value == _tareInfoText) return;
                _tareInfoText = value;
                NotifyOfPropertyChange(() => TareInfoText);
            }
        }

        public bool TareError
        {
            get { return _tareError; }
            set
            {
                if (value.Equals(_tareError)) return;
                _tareError = value;
                NotifyOfPropertyChange(() => TareError);
            }
        }

        public void Handle(ViewModelMessage message)
        {
            if (message.MessageType == "ERROR")
            {
                TareInfoText =
                    Resource.Resource.TareViewModel_Handle_;
                TareError = true;
            }
        }

        public void RequestTare()
        {
            ViewModelEventAggregator.EventAggregator.Publish(new TareRequestMessage { Sender = GetType() });
        }
    }
}
