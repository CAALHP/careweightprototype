﻿using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Timers;
using CareWeight.ViewModelEvents;
using Caliburn.Micro;
using CareWeight.Resource;


namespace CareWeight.ViewModels
{
    [Export(typeof(StatusBarViewModel))]
    public class StatusBarViewModel : BaseViewModel, IHandle<StatusMessage>
    {
        #region fields

        private string _status;
        private Timer _timer = new Timer(60000);
        private string _time;
        private string _date;
        private BaseViewModel _buttonView;

        #endregion
        
        #region constructor

        public StatusBarViewModel()
        {
            Name = "StatusBar";

            ButtonView = new SettingsViewModel();

            Date = DateTime.Now.Date.ToShortDateString();
            Time = DateTime.Now.ToString("HH:mm");

            _timer.Elapsed += _timer_Elapsed;

            _timer.Start();

            ViewModelEventAggregator.EventAggregator.Subscribe(this);
        }

        #endregion
        
        #region eventhandlers

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Date = DateTime.Now.Date.ToShortDateString();
            Time = DateTime.Now.ToString("HH:mm");
        }

        #endregion

        #region IHandle

        public void Handle(StatusMessage message)
        {
            Status = message.Status;
        }

        #endregion
        
        #region properties

        public string Date
        {
            get { return _date; }
            set
            {
                if (value == _date) return;
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }

        public string Time
        {
            get { return _time; }
            set
            {
                if (value == _time) return;
                _time = value;
                NotifyOfPropertyChange(() => Time);
            }
        }

        public string Status
        {
            get { return Resource.Resource.StatusBarViewModel_Status_Weight_Status__ + _status; }
            set
            {
                if (value == _status) return;
                _status = value;
                //_status = Resources.StatusBarViewModel_Status_Weight_Status__;
                NotifyOfPropertyChange(() => Status);
            }
        }

        public string CenterId
        {
            get { return ConfigurationManager.AppSettings["center"]; }
        }

        public BaseViewModel ButtonView
        {
            get { return _buttonView; }
            set
            {
                if (Equals(value, _buttonView)) return;
                _buttonView = value;
                NotifyOfPropertyChange(() => ButtonView);
            }
        }

        #endregion

        #region methods

        public void Quit()
        {
            ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Quit"});
        }

        public void Minimize()
        {
            ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Minimize"});
        }

        #endregion

     
    }
}
