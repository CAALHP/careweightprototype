﻿using System.ComponentModel.Composition;
using CareWeight.State;
using CareWeight.ViewModelEvents;
using Caliburn.Micro;

namespace CareWeight.ViewModels
{
    [Export(typeof(BaseViewModel))]
    public abstract class BaseViewModel : PropertyChangedBase
    {
        #region fields

        private string _name;
        private string _statusMessage;

        #endregion

        #region properties

        public string Name
        {
            get { return _name; }
            protected set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                _statusMessage = value;
                SendStatusMessage();
            }
        }

        #endregion
        
        #region methods

        protected void SendStatusMessage()
        {
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() {Status = StatusMessage});
        }

        /// <summary>
        /// updates the controls of the current view to syncronice with viewmodel
        /// Should be used if resources are used for localization in the viewmodel.
        /// </summary>
        public void UpdateView()
        {
            NotifyOfPropertyChange(string.Empty);
        }

        #endregion

    }
}
