﻿using System.Configuration;
using Caliburn.Micro;

namespace CareWeight.ViewModels
{
    public class SetCenterDialogViewModel: Screen
    {
        #region fields

        private string _centerNameText;
        private TouchKeyboardViewModel _touchKeyboard = new TouchKeyboardViewModel();

        #endregion
        
        #region properties

        public bool CanStore
        {
            get { return !string.IsNullOrEmpty(_centerNameText); }
        }



        public TouchKeyboardViewModel TouchKeyboard
        {
            get { return _touchKeyboard; }
            set
            {
                _touchKeyboard = value;
                NotifyOfPropertyChange(() => TouchKeyboard);
            }
        }

        public string CenterNameText
        {
            get { return _centerNameText; }
            set
            {
                if (value == _centerNameText) return;
                _centerNameText = value;
                NotifyOfPropertyChange(() => CenterNameText);
                NotifyOfPropertyChange(() => CanStore);
            }
        }

        #endregion
        
        #region Methods

        public void Store()
        {
            Configuration config =
                ConfigurationManager.OpenExeConfiguration
                    (ConfigurationUserLevel.None);

            //sets the center name in the app config to be used when creating identification
            config.AppSettings.Settings["center"].Value = _centerNameText;
            //saves only the changes values in the config xml doc.
            //config.Save(ConfigurationSaveMode.Modified);
            config.Save();
            //force config manager to refresh new values, else changes wont be detected at runtime.
            ConfigurationManager.RefreshSection("appSettings");

            TryClose();

        }

        #endregion

    }
}
