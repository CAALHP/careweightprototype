﻿using System.ComponentModel.Composition;
using CareWeight.Resource;
using CareWeight.ViewModels;

namespace CareWeight.ViewModels
{
    [Export(typeof(StartViewModel))]
    public class StartViewModel : BaseViewModel
    {

        public StartViewModel()
        {
            Name = "Start";

           // StatusMessage = Resource.Resource.StartViewModel_StartViewModel_Klar_;

        }
    }
}
