﻿using System.ComponentModel.Composition;
using CAALHP.Events.Types;
using CareWeight.ViewModelEvents;
using CareWeight.Resource;
using DataTypeLibrary;
using Data;

namespace CareWeight.ViewModels
{
    [Export(typeof(StatusViewModel))]
    public class StatusViewModel : BaseViewModel
    {

        #region fields

        private string _personName;
        private double _personWeight;
        private Person _person;
        private string _quality = null;
        private UserProfile _currentLoggedUser;
        //  private WcfClient wcfClient = new WcfClient();

        #endregion

        #region constructor

        public StatusViewModel(UserProfile user, double weight)
        {
            Name = "Status";

            _currentLoggedUser = user;
            //_person = DataWrapper.Instance.GetPerson(cpr);
            //if (_person != null)
            //    PersonName = _person.Name;

            PersonWeight = weight;

            StatusMessage = Resource.Resource.StatusViewModel_StatusViewModel_Vejning_gennemført_;
        }

        #endregion
        
        #region properties

        public string PersonName
        {
            get { return _personName; }
            set
            {
                _personName = value;
                NotifyOfPropertyChange(() => PersonName);
            }
        }

        public double PersonWeight
        {
            get { return _personWeight; }
            set
            {
                _personWeight = value;
                NotifyOfPropertyChange(() => PersonWeight);
            }
        }

        #endregion

        #region methods

        public void RejectWeight()
        {
            _quality = "rejected";
            //sets person to null, wont be send to the webservice. 
            _person = null;
            Store();
            //ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() { MessageType = Name + " Done" });
            ViewModelEventAggregator.EventAggregator.Publish(new RejectWeightMessage() { Sender = GetType() });
        }

        public void Reweight()
        {
            _quality = "reweigh";
            Store();
            //ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Reweigh"});
            ViewModelEventAggregator.EventAggregator.Publish(new ReweighMessage() { Sender = GetType() });
        }

        public void Done()
        {
            Store();
            //ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Done"});
            ViewModelEventAggregator.EventAggregator.Publish(new AcceptWeightMessage() { Sender = GetType()});
        }

        public void Tare()
        {
            _quality = "tare";
            Store();
        }

        private void Store()
        {
            //todo refactor this to match new pattern for storing when users gets to this point, maybe do it in the constructor.
            //DataWrapper.Instance.NewMeasurement("Weight", _personWeight, _person, _quality);
            //TODO ADD Quality as factor.
            ViewModelEventAggregator.EventAggregator.Publish(new WeightMessage() { Value = _personWeight, Sender = GetType(), User = _currentLoggedUser });

            StatusMessage = "";
        }

        #endregion

    }
}
