﻿using System.Timers;
using CareWeight.Resource;

namespace CareWeight.ViewModels
{
    public class WeighingViewModel : BaseViewModel
    {

        #region fields

        private readonly Timer timer;
        private int _counter;

        #endregion
        
        #region constructor

        public WeighingViewModel()
        {
            Name = "Weighing";

            timer = new Timer(1000);

            Counter = 0;

            StatusMessage = Resource.Resource.WeighingViewModel_WeighingViewModel_Beregner_vægt_;
        }

        #endregion
        
        #region methods

        public void Start()
        {
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
            timer.Elapsed -= timer_Elapsed;
        }

        #endregion
        
        #region eventhandler

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Counter == 5)
                Counter = 0;
            else
                Counter++;
        }

        #endregion

        #region properties

        private int Counter
        {
            get { return _counter; }
            set
            {
                _counter = value;
                NotifyOfPropertyChange(() => FirstDotVisible);
                NotifyOfPropertyChange(() => SecondDotVisible);
                NotifyOfPropertyChange(() => TheirdDotVisible);
            }
        }

        public bool FirstDotVisible
        {
            get { return Counter > 0 && Counter < 4; }
        }

        public bool SecondDotVisible
        {
            get { return Counter >= 2 && Counter <= 4; }
        }

        public bool TheirdDotVisible
        {
            get { return Counter >= 3; }
        }

        #endregion

    }
    
}
