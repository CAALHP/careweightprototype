﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;


namespace CareWeight.ViewModels
{
   // [Export(typeof(TouchKeyboardViewModel))]
    public sealed class TouchKeyboardViewModel: BaseViewModel
    {
        #region fields

        private readonly BindableCollection<char> textKeys01 = new BindableCollection<char>
            {
                'Q',
                'W',
                'E',
                'R',
                'T',
                'Y',
                'U',
                'I',
                'O',
                'P',
                'Å'
            };

        private readonly BindableCollection<char> textKeys02 = new BindableCollection<char>
            {
                'A',
                'S',
                'D',
                'F',
                'G',
                'H',
                'J',
                'K',
                'L',
                'Æ',
                'Ø'
            };

        private readonly BindableCollection<char> textKeys03 = new BindableCollection<char>
            {
                'Z',
                'X',
                'C',
                'V',
                'B',
                'N',
                'M'
            };

        private readonly BindableCollection<char> numericKeys01 = new BindableCollection<char>
            {
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '0'
            };

        private readonly BindableCollection<char> numericKeys02 = new BindableCollection<char>
            {
                '-',
                '/',
                ':',
                ';',
                '(',
                ')',
                '€',
                '&',
                '§'
            };

        private readonly BindableCollection<char> numericKeys03 = new BindableCollection<char>
            {
                '.',
                ',',
                '?',
                '!',
                '\'',
                '"',
                '@'
            };

        private readonly BindableCollection<char> signKeys01 = new BindableCollection<char>
            {
                '[',
                ']',
                '{',
                '}',
                '#',
                '%',
                '^',
                '*',
                '+',
                '='
            };

        private readonly BindableCollection<char> signKeys02 = new BindableCollection<char>
            {
                '_',
                '\\',
                '|',
                '~',
                '<',
                '>',
                '$',
                '£',
                '°'
            };

        private readonly BindableCollection<char> signKeys03 = new BindableCollection<char>
            {
                '.',
                ',',
                '?',
                '!',
                '\'',
                '"',
                '@'
            };

        private BindableCollection<char> keys01;
        private BindableCollection<char> keys02;
        private BindableCollection<char> keys03;

        #endregion
        
        #region constructor

        public TouchKeyboardViewModel()
        {
            Name = "Keyboard";

            Keys01 = textKeys01;
            Keys02 = textKeys02;
            Keys03 = textKeys03;

        }

        #endregion
        
        #region properties

        public BindableCollection<char> Keys01
        {
            get { return keys01; }
            set
            {
                keys01 = value;
                NotifyOfPropertyChange(() => Keys01);
            }
        }

        public BindableCollection<char> Keys02
        {
            get { return keys02; }
            set
            {
                keys02 = value;
                NotifyOfPropertyChange(() => Keys02);
            }
        }

        public BindableCollection<char> Keys03
        {
            get { return keys03; }
            set
            {
                keys03 = value;
                NotifyOfPropertyChange(() => Keys03);
            }
        }


        private bool isShiftPressed;

        public bool IsShiftPressed
        {
            get { return isShiftPressed; }
            set
            {
                isShiftPressed = value;
                NotifyOfPropertyChange(() => IsShiftPressed);
            }
        }


        private bool isSignPressed;

        public bool IsSignPressed
        {
            get { return isSignPressed; }
            set
            {
                isSignPressed = value;
                NotifyOfPropertyChange(() => IsShiftPressed);
            }
        }

        private bool isNumericPressed;

        public bool IsNumericPressed
        {
            get { return isNumericPressed; }
            set
            {
                isNumericPressed = value;
                NotifyOfPropertyChange(() => IsNumericPressed);
            }
        }

        #endregion
        
        #region methods

        public void NormalKey(object sender)
        {
            string character = sender.ToString();

            if (IsShiftPressed)
            {
                character = character.ToUpperInvariant();
            }
            else
            {
                character = character.ToLowerInvariant();
            }

            TextComposition text = new TextComposition(InputManager.Current, Keyboard.FocusedElement, character);
            TextCompositionEventArgs e = new TextCompositionEventArgs(Keyboard.PrimaryDevice, text) { RoutedEvent = UIElement.TextInputEvent };

            InputManager.Current.ProcessInput(e);

            IsShiftPressed = false;
        }

        public void NumericLayout(bool state)
        {
            IsNumericPressed = state;
            SetKeyboardLayout();
        }

        public void SpecialKey(object sender)
        {
            string tag = ((Button) sender).Tag.ToString();

            IsShiftPressed = false;
            if (tag == "Numeric")
            {
                IsNumericPressed = !IsNumericPressed;
                IsSignPressed = false;

                SetKeyboardLayout();
            }
            else if (tag == "Sign")
            {
                IsSignPressed = !IsSignPressed;
                IsNumericPressed = false;

                if (IsSignPressed)
                {
                    Keys01 = signKeys01;
                    Keys02 = signKeys02;
                    Keys03 = signKeys03;
                }
                else
                {
                    Keys01 = textKeys01;
                    Keys02 = textKeys02;
                    Keys03 = textKeys03;
                }

            }
            else if (tag == "Shift")
            {
                IsShiftPressed = true;
            }
            else
            {
                KeyConverter converter = new KeyConverter();
                Key pressedKey = (Key) converter.ConvertFrom(tag);

                KeyEventArgs e = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0,
                                                  pressedKey) {RoutedEvent = Keyboard.KeyDownEvent};
                InputManager.Current.ProcessInput(e);
            }
        }

        private void SetKeyboardLayout()
        {
            if (IsNumericPressed)
            {
                Keys01 = numericKeys01;
                Keys02 = numericKeys02;
                Keys03 = numericKeys03;
            }
            else
            {
                Keys01 = textKeys01;
                Keys02 = textKeys02;
                Keys03 = textKeys03;
            }
        }

        #endregion

    }
    
}
