﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using CAALHP.Events;
using CAALHP.Events.UserServiceEvents;
using CW.Events;
using CareWeight.State;
using CareWeight.Tools;
using CareWeight.ViewModelEvents;
using Caliburn.Micro;
using CareWeight.Resource;
using Data;
using DataTypeLibrary;
using FaceIdentificationAdaptor;
using WeightInterface;
using Action = System.Action;

namespace CareWeight.ViewModels
{
    [Export(typeof(IShell))]
    public class ShellViewModel : Screen, IShell, IHandle<ViewModelMessage>, IHandle<BaseMessage>
    {
        #region private fields
        //Conductor<Screen>.Collection.AllActive

        private Weight _weight;
        private object _lock = new object();
        readonly IWindowManager _windowManager;
        private int _tarecounter;

        #endregion

        #region constructor
        //Todo implement dispose pattern. unsubscripe alle events and dispose weight and viewmodels needed.
        [ImportingConstructor]
        public ShellViewModel(IWindowManager windowManager)
        {

            _windowManager = windowManager;

            LanguageManager.Init();

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            Application.Current.Exit += Current_Exit;

            //Sets the Initial state
            State = new StartState(this);

            Status = new StatusBarViewModel();

            //Body = new StartViewModel();

            //SetBodyView("Start");

            //subscribes to viewmodelevents
            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Status = Resource.Resource.ShellViewModel_ShellViewModel_Venter_paa_forbindelse_til_vægt });

            //Show();

        }

        #endregion

        #region Event handlers

        void Current_Exit(object sender, ExitEventArgs e)
        {
            _weight.Dispose();
            Application.Current.DispatcherUnhandledException -= Current_DispatcherUnhandledException;
        }

        void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            //forces thread to change to da-DK format fore storing log.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");

            //writes unhandled exceptions into logfil and closes down application.
            var path = @"c:\Log\Exceptions\";

            //  if (Directory.Exists(path))
            Directory.CreateDirectory(path);


            string filename = path + DateTime.Now.Date.ToShortDateString() + ".txt";
            //writes unhandled exceptions into logfil and closes down application.
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true))
            {
                file.Write(DateTime.Now.ToShortTimeString() + " ");
                file.WriteLine(e.Exception.ToString());

            }

            Application.Current.Shutdown();
        }

        //done
        void _weight_OnWeightDataReady(object sender, WeightEventArgs e)
        {

            RecievedWeight = e.Buffer;

            lock (_lock)
            {
                if (Body.Name != "Weighing") return;
            }

            SetBodyView("Status");

        }

        void _weight_OnUserOffWeight(object sender, EventArgs e)
        {

            BaseViewModel currentbody;

            lock (_lock)
            {
                //gets a local refrence to body, so further locking is not needed.
                currentbody = Body;
            }

            //if currentbody is status, ensure that data is saved even if user didnt press a button
            if (currentbody.Name == "Status")
            {
                ((StatusViewModel)currentbody).Done();
            }
            else if (currentbody.Name == "Tare")
            {
                //Initiates tare when user steps off weight.
                _weight.OnTareCompleted += WeightOnTareCompleted;

                _weight.ManualTare();

                ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage() { Status = Resource.Resource.ShellViewModel__weight_OnUserOffWeight_Vægten_tarerer_ });
            }
            else
            {
                SetBodyView("Start");
            }

        }

        void _weight_OnUserOnWeight(object sender, EventArgs e)
        {
            lock (_lock)
            {
                if (Body.Name == "Tare") return;
            }
            //Dispatcher.CurrentDispatcher.BeginInvoke( (System.Action)( ()=> SetBodyView("Face")));

            SetBodyView("Face");

            //resets recieved wight to 0 for new measurement
            RecievedWeight = 0;

        }

        private void WeightOnTareCompleted(object sender, TareEventArgs tareEventArgs)
        {
            lock (_lock)
            {
                if (Body.Name != "Tare") return;
            }

            if (tareEventArgs.Buffer == "OK")
            {
                //reset the tare counter.
                _tarecounter = 0;

                _weight.OnTareCompleted -= WeightOnTareCompleted;

                SetBodyView("Start");

            }
            if (tareEventArgs.Buffer == "ERROR")
            {
                //trys to tare 5 times if error is recieved, before passing error to user.
                if (_tarecounter < 5)
                {
                    _weight.ManualTare();
                    _tarecounter++;
                }
                else
                {
                    ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() { MessageType = tareEventArgs.Buffer });
                }
            }
        }

        #endregion

        #region public properties

        public double RecievedWeight { get; set; }
        public ICareWeightState State { get; set; }

        private BaseViewModel _status;
        public BaseViewModel Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyOfPropertyChange(() => Status);

            }
        }

        private BaseViewModel _body;
        public BaseViewModel Body
        {
            get { return _body; }
            set
            {
                _body = value;
                NotifyOfPropertyChange(() => Body);

            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// if center is not set, blocks program untill set.
        /// </summary>
        public void ViewLoaded()
        {
            //Debugger.Launch();
            //TODO remove after test is done
            Keyboard.AddPreviewKeyDownHandler(Application.Current.MainWindow, TestPreviewKeyDown);

            //checks if the centername has been set in app.config, if null opens dialog to set value.
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["center"])) return;

            string result;
            do
            {
                //shows dialog for setcentername
                _windowManager.ShowDialog(new SetCenterDialogViewModel());
                result = ConfigurationManager.AppSettings["center"];

            } while (string.IsNullOrEmpty(result));
            //updates status view to get centername.
            Status.UpdateView();

        }


        private void UnsubscribeToWeightEvets()
        {
            if (_weight != null)
            {
                _weight.OnUserOffWeight -= _weight_OnUserOffWeight;
                _weight.OnUserOnWeight -= _weight_OnUserOnWeight;
                _weight.OnWeightDataReady -= _weight_OnWeightDataReady;
            }
        }

        private void ShowLanguageDialog()
        {
            _windowManager.ShowDialog(new ChangeLanguageViewModel());
            _body.UpdateView();
            // _banner.UpdateView();
            _status.UpdateView();
            // NotifyOfPropertyChange(string.Empty);
        }

        private void SetBodyView(string viewName)
        {
            string idresult = "";
            BaseViewModel tmpviewmodel;
            if (viewName == "Status")
                idresult = FaceIdentificationFacade.Instance.LastResult;


            switch (viewName)
            {
                case "Face":
                    tmpviewmodel = new IdentificationViewModel();
                    break;
                //case "NewUser":
                //   // tmpviewmodel = new NewPersonViewModel();
                //    break;
                case "Status":
                    //sets the status view with result form last identification
                    //tmpviewmodel = new StatusViewModel(idresult, RecievedWeight);
                    tmpviewmodel = new StatusViewModel(null, RecievedWeight);
                    break;
                case "Tare":
                    tmpviewmodel = new TareViewModel();
                    break;
                case "Weighing":
                    tmpviewmodel = new WeighingViewModel();
                    break;
                default:
                    tmpviewmodel = new StartViewModel();
                    break;
            }

            lock (_lock)
            {
                if (Body != null && Body.Name == "Tare" && tmpviewmodel.Name != "Tare" && tmpviewmodel.Name != "Start") return;

                Body = tmpviewmodel;
            }

        }

        private void Minimize()
        {
            Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        private void Quit()
        {
            Application.Current.Shutdown();
        }

        private void Show()
        {
            //   MessageBox.Show("Show is called");
            Application.Current.MainWindow.WindowState = WindowState.Minimized;
            Application.Current.MainWindow.WindowState = WindowState.Maximized;

            //_windowManager.ShowWindow(this);

            //  this.ActivateItem(this);

        }

        #endregion

        #region IHandle implementation

        public void Handle(ViewModelMessage message)
        {
            string msg = message.MessageType;

            switch (msg)
            {
                case "Status Done":
                    SetBodyView("Start");
                    break;
                case "Status Reweigh":
                    _weight.ManualWeighing();
                    SetBodyView("Weighing");
                    break;
                case "Banner Tare":
                    BaseViewModel currentbody;
                    lock (_lock)
                    {
                        currentbody = Body;
                    }
                    //explisit calls tare on statusviewmodel to ensure that data is stored with tare as quality
                    if (currentbody.Name == "Status")
                        ((StatusViewModel)currentbody).Tare();
                    SetBodyView("Tare");
                    break;
                case "Banner Language":
                    ShowLanguageDialog();
                    break;
                case "Identification Continue":
                    //race condition, if weight is not done switches to weighing to avoid blocking or deadlock
                    SetBodyView(RecievedWeight == 0 ? "Weighing" : "Status");
                    break;
                case "Identification NewUser":
                    SetBodyView("NewUser");
                    break;
                case "NewPerson Done":
                    //race condition, if weight is not done switches to weighing to avoid blocking or deadlock
                    SetBodyView(RecievedWeight == 0 ? "Weighing" : "Status");
                    break;
                case "StatusBar Quit":
                    Quit();
                    break;
                case "StatusBar Minimize":
                    Minimize();
                    break;
                case "Show":
                    Show();
                    break;
            }
        }

        #endregion

        public void HandleMessage(UserOnWeightMessage message)
        {
            State.UserOnWeight();
        }

        public void HandleMessage(UserOffWeightMessage message)
        {
            State.UserOffWeight();
        }

        public void HandleMessage(WeightDataReadyMessage message)
        {
            State.WeightDataReady(message.Value);
        }

        public void HandleMessage(IdentificationDoneMessage message)
        {

            State.FacialIdentificationDone(message.User);
        }

        public void HandleMessage(TareMessage message)
        {
            State.Tare();
        }

        public void HandleMessage(TareCompleteMessage message)
        {
            State.TareComplete();
        }

        public void HandleMessage(AcceptWeightMessage message)
        {
            State.WeightAccepted();
        }

        public void HandleMessage(ReweighMessage message)
        {
            State.Reweigh();
        }

        public void HandleMessage(RejectWeightMessage message)
        {
            State.Reject();
        }

        private void HandleMessage(BaseMessage message)
        {
            //This function is ment to be empty by default
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
            //MessageBox.Show("BaseMessage handler called.");
        }



        #region Test

        private void TestPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var keyPressed = e.Key;

            switch (keyPressed)
            {
                case Key.NumPad0:
                    break;
                case Key.X:
                    //generates a random weight between 65 and 85 kg.
                    Random random = new Random();
                    var randomWeight = random.NextDouble() * (85 - 65) + 65;
                    HandleCaalphEvent(new WeightDataReadyEvent() { Value = Math.Round(randomWeight, 1) });
                    e.Handled = true;
                    break;
                case Key.C:
                    HandleCaalphEvent(new UserOnWeightEvent());
                    e.Handled = true;
                    break;
                case Key.V:
                    HandleCaalphEvent(new UserOffWeightEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad4:
                    HandleCaalphEvent(new UserLoggedInEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad5:
                    HandleCaalphEvent(new FacialAuthFailedResponsEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad6:
                    HandleCaalphEvent(new FacialErrorEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad7:
                    HandleCaalphEvent(new ConnectionErrorEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad8:
                    HandleCaalphEvent(new TareResponsEvent());
                    e.Handled = true;
                    break;
                case Key.NumPad9:
                    HandleCaalphEvent(new TareErrorResponsEvent());
                    e.Handled = true;
                    break;
                case Key.Multiply:
                    break;
                case Key.Add:
                    break;
                case Key.Separator:
                    break;
                case Key.Subtract:
                    break;
                case Key.Decimal:
                    break;
                case Key.Divide:
                    break;
                default:
                    //throw new ArgumentOutOfRangeException();
                    break;
            }
        }

        private void HandleCaalphEvent(ConnectionErrorEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_NoConnectionError });
        }

        private void HandleCaalphEvent(TareErrorResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_Der_skete_en_fejl_under_nulstilling_af_vægten_ });
        }

        private void HandleCaalphEvent(TareResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new TareCompleteMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(UserOffWeightEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new UserOffWeightMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(UserOnWeightEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new UserOnWeightMessage { Sender = GetType() });
            //throw new NotImplementedException();
        }

        private void HandleCaalphEvent(WeightDataReadyEvent o)
        {
            var value = o.Value;
            ViewModelEventAggregator.EventAggregator.Publish(new WeightDataReadyMessage { Sender = GetType(), Value = value });
        }

        private void HandleCaalphEvent(UserLoggedInEvent o)
        {
            //TODO skal vi bruge brugernavn til noget.
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationDoneMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(FacialAuthFailedResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationFailedMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(FacialErrorEvent o)
        {
            var message = o.Message;
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_IdentificationFailed + message });
        }


        #endregion
    }
}
