﻿using System.Collections.Generic;
using System.Globalization;
using CareWeight.Tools;
using Caliburn.Micro;

namespace CareWeight.ViewModels
{
    public class ChangeLanguageViewModel:Screen
    {

        #region constructor

        public ChangeLanguageViewModel()
        {
            Selectedlng = LanguageManager.GetIndex(CultureInfo.CurrentUICulture.Name);
        }

        #endregion
        
        #region properties

        public List<CultureInfo> CultList
        {
            get { return LanguageManager.CultList; }
        }

        public int Selectedlng { get; set; }

        #endregion

        #region methods

        /// <summary>
        /// Changes the the settings to the selected, raises event to update all UI controls. 
        /// </summary>
        public void ChangelangOk()
        {
            if (LanguageManager.ChangeLanguage(Selectedlng))
            {
                NotifyOfPropertyChange(string.Empty);
            }

            TryClose();
        }

        #endregion

    }
}
