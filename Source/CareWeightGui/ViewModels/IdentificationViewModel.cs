﻿using System.ComponentModel.Composition;
using System.Threading;
using Caliburn.Micro;
using CareWeight.ViewModelEvents;
using CareWeight.Resource;
using CareWeight.ViewModels;
using FaceIdentificationAdaptor;
using Timer = System.Timers.Timer;

namespace CareWeight.ViewModels
{
    [Export(typeof(IdentificationViewModel))]
    public class IdentificationViewModel : BaseViewModel, IHandle<IdentificationFailedMessage>
    {
        #region fields

        private bool _isIdComplete;
         
        private string _infotext;
        // private Timer _timeoutTimer = new Timer(10000);
        private Timer _progressbarTimer = new Timer(1000);
        private int _counter;
        private bool _isIdRunning;

        #endregion

        #region constructor

        public IdentificationViewModel()
        {
            // Name = this.GetType().ToString();
            IdentificationRequest();

            Name = "Identification";

            Counter = 0;

            _progressbarTimer.Elapsed += ProgressbarTimerElapsed;

            ViewModelEventAggregator.EventAggregator.Subscribe(this);
            
            //_timeoutTimer.Elapsed += TimeoutTimerElapsed;
            //_timeoutTimer.AutoReset = false;

            IsIdComplete = false;

            Infotext = Resource.Resource.FaceViewModel_FaceViewModel_Kig_her_mens_vi_prøver_at_identificere_dig;

            StatusMessage = Resource.Resource.IdentificationViewModel_IdentificationViewModel_Afventer_Identification_;
        }


        #endregion
        
        #region properties

        public string Infotext
        {
            get { return _infotext; }
            set
            {
                if (value == _infotext) return;
                _infotext = value;
                NotifyOfPropertyChange(() => Infotext);
            }
        }

        public bool IsIdComplete
        {
            get { return _isIdComplete; }
            set
            {
                if (value == _isIdComplete) return;
                _isIdComplete = value;
                NotifyOfPropertyChange(() => IsIdComplete);
            }
        }

        public bool IsIdRunning
        {
            get { return _isIdRunning; }
            set
            {
                if (value.Equals(_isIdRunning)) return;
                _isIdRunning = value;
                NotifyOfPropertyChange(() => IsIdRunning);
            }
        }

        private int Counter
        {
            get { return _counter; }
            set
            {
                _counter = value;
                NotifyOfPropertyChange(() => FirstDotVisible);
                NotifyOfPropertyChange(() => SecondDotVisible);
                NotifyOfPropertyChange(() => TheirdDotVisible);
            }
        }

        public bool FirstDotVisible
        {
            get { return Counter > 0 && Counter < 4; }
        }

        public bool SecondDotVisible
        {
            get { return Counter >= 2 && Counter <= 4; }
        }

        public bool TheirdDotVisible
        {
            get { return Counter >= 3; }
        }


        #endregion
        
        #region Methods

        /// <summary>
        /// Starts identifying when view is loaded.
        /// </summary>
        public void ViewLoaded()
        {
            //Thread myThread = new Thread(Identify);
            //myThread.Start();
            //_timeoutTimer.Stop();
            //_timeoutTimer.Start();
            
        }

        //public void ViewUnloaded()
        //{
        //    if (FaceIdentificationFacade.Instance.IsIdentifyingRunning)
        //    {
        //        FaceIdentificationFacade.Instance.IsIdentifyingRunning = false;
        //        _timeoutTimer.Stop();
        //    }
        //}

        //public void ExistingUser()
        //{
        //    ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Continue"});
        //}

        //public void NewUser()
        //{
        //    ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " NewUser"});
        //}

        public void ContinueWithOutId()
        {
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationDoneMessage() { Sender = GetType()});
        }

        public void IdentificationRequest()
        {
            IsIdComplete = false;
            IsIdRunning = true;

            _progressbarTimer.Start();

            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationRequestMessage() { Sender = GetType()});
        }

        private void ProgressbarTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Counter == 5)
                Counter = 0;
            else
                Counter++;
        }
        
        #endregion

        public void Handle(IdentificationFailedMessage message)
        {
            _progressbarTimer.Stop();
            
            //Counter = 3;

            IsIdComplete = true;
            IsIdRunning = false;

            Infotext = Resource.Resource.FaceViewModel_Identify_Recognition_Failed_;
        }
    }
}
