﻿using System.ComponentModel.Composition;
using CareWeight.ViewModelEvents;

namespace CareWeight.ViewModels
{
    [Export(typeof(SettingsViewModel))]
    public class SettingsViewModel : BaseViewModel
    {
        #region constructor

        public SettingsViewModel()
        {
            Name = "Banner";
        }

        #endregion
        
        #region methods


        public void Tare()
        {
            // ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage() {MessageType = Name + " Tare"});
            // ViewModelEventAggregator.EventAggregator.Publish(new UserOnWeightMessage { Sender = GetType() });

            ViewModelEventAggregator.EventAggregator.Publish(new TareMessage { });
        }

        #endregion
        
    }
}
