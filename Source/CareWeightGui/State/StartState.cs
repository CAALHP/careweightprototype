﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareWeight.ViewModelEvents;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public class StartState: CareWeightBaseState
    {
        public StartState(ShellViewModel shellview) : base(shellview)
        {
            Shellview.Body = new StartViewModel();
        }

        public override void UserOnWeight()
        {
            Shellview.RecievedWeight = 0;
            Shellview.State = new IdentificationState(Shellview);
        }

        public override void UserOffWeight()
        {
            //empty function by statepattern design
        }

        public override void WeightDataReady(double value)
        {
            //empty function by statepattern design
        }

        public override void FacialIdentificationDone(UserProfile user)
        {
            //empty function by statepattern design
        }

        public override void Tare()
        {
            Shellview.State = new TareState(Shellview);
        }

        public override void TareComplete()
        {
            //empty function by statepattern design
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = "Klar" });
        }

        public override void WeightAccepted()
        {
            //empty function by statepattern design
        }

        public override void Reweigh()
        {
            //empty function by statepattern design
        }

        public override void Reject()
        {
            //empty function by statepattern design
        }
    }
}
