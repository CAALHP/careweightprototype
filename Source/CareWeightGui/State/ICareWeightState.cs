﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CareWeight.State
{
    public interface ICareWeightState
    {
        void UserOnWeight();
        void UserOffWeight();
        void WeightDataReady(double value);
        void FacialIdentificationDone(UserProfile user);
        void Tare();
        void TareComplete();
        void WeightAccepted();
        void Reweigh();
        void Reject();
        
    }
}
