﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public class WeighingState : CareWeightBaseState
    {
        private readonly UserProfile _user;

        public WeighingState(ShellViewModel shellview, UserProfile user) : base(shellview)
        {
            _user = user;
            Shellview.Body = new WeighingViewModel();
        }

        public override void UserOnWeight()
        {
            //empty function by statepattern design
        }

        public override void UserOffWeight()
        {
            Shellview.State = new StartState(Shellview);
        }

        public override void WeightDataReady(double value)
        {
            Shellview.State = new StatusState(Shellview, value, _user);
        }

        public override void FacialIdentificationDone(UserProfile user)
        {
            //empty function by statepattern design
        }

        public override void Tare()
        {
            Shellview.State = new TareState(Shellview);
        }

        public override void TareComplete()
        {
            //empty function by statepattern design
        }

        public override void WeightAccepted()
        {
            //empty function by statepattern design
        }

        public override void Reweigh()
        {
            //empty function by statepattern design
        }

        public override void Reject()
        {
            //empty function by statepattern design
        }
    }
}
