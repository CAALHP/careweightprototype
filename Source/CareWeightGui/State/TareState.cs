﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public class TareState: CareWeightBaseState
    {
        public TareState(ShellViewModel shellview) : base(shellview)
        {
            Shellview.Body = new TareViewModel();
        }

        public override void UserOnWeight()
        {
            //empty function by statepattern design
        }

        public override void UserOffWeight()
        {
            ((TareViewModel)Shellview.Body).RequestTare();
        }

        public override void WeightDataReady(double value)
        {
            //empty function by statepattern design
        }

        public override void FacialIdentificationDone(UserProfile user)
        {
            //empty function by statepattern design
        }

        public override void Tare()
        {
            //empty function by statepattern design
        }

        public override void TareComplete()
        {
            Shellview.State = new StartState(Shellview);
        }

        public override void WeightAccepted()
        {
            //empty function by statepattern design
        }

        public override void Reweigh()
        {
            //empty function by statepattern design
        }

        public override void Reject()
        {
            //empty function by statepattern design
        }
    }
}
