﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public abstract class CareWeightBaseState: ICareWeightState
    {
        protected readonly ShellViewModel Shellview;

        protected CareWeightBaseState(ShellViewModel shellview)
        {
            Shellview = shellview;
        }

        public abstract void UserOnWeight();
        public abstract void UserOffWeight();
        public abstract void WeightDataReady(double value);
        public abstract void FacialIdentificationDone(UserProfile user);
        public abstract void Tare();
        public abstract void TareComplete();
        public abstract void WeightAccepted();
        public abstract void Reweigh();
        public abstract void Reject();
    }
}
