﻿using CAALHP.Events.Types;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public class IdentificationState : CareWeightBaseState
    {

        public IdentificationState(ShellViewModel shellViewModel)
            : base(shellViewModel)
        {
            shellViewModel.Body = new IdentificationViewModel();
        }

        public override void UserOnWeight()
        {
            //empty function by statepattern design
        }

        public override void UserOffWeight()
        {
            Shellview.State = new StartState(Shellview);
        }

        public override void WeightDataReady(double value)
        {
            Shellview.RecievedWeight = value;
            
        }

        public override void FacialIdentificationDone(UserProfile user)
        {
            //todo is this the right place to do this.
            if (Shellview.RecievedWeight == 0)
                Shellview.State = new WeighingState(Shellview, user); 
            else
                Shellview.State = new StatusState(Shellview, Shellview.RecievedWeight, user);
        }

        public override void Tare()
        {
            Shellview.State = new TareState(Shellview);
        }

        public override void TareComplete()
        {
            //empty function by statepattern design
        }

        public override void WeightAccepted()
        {
            //empty function by statepattern design
        }

        public override void Reweigh()
        {
            //empty function by statepattern design
        }

        public override void Reject()
        {
            //empty function by statepattern design
        }
    }
}