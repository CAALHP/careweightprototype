﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;
using CareWeight.ViewModels;

namespace CareWeight.State
{
    public class StatusState : CareWeightBaseState
    {
        private UserProfile _user;

        public StatusState(ShellViewModel shellview, double measurement, UserProfile user)
            : base(shellview)
        {

            _user = user;
            Shellview.Body = new StatusViewModel(_user, measurement);
        }

        public override void UserOnWeight()
        {
            //empty function by statepattern design
        }

        public override void UserOffWeight()
        {
            ((StatusViewModel)Shellview.Body).Done();

            Shellview.State = new StartState(Shellview);
        }

        public override void WeightDataReady(double value)
        {
            //empty function by statepattern design
        }

        public override void FacialIdentificationDone(UserProfile user)
        {
            //empty function by statepattern design
        }

        public override void Tare()
        {
            ((StatusViewModel)Shellview.Body).Tare();

            Shellview.State = new TareState(Shellview);
        }

        public override void TareComplete()
        {
            //empty function by statepattern design
        }

        public override void WeightAccepted()
        {
            Shellview.State = new StartState(Shellview);
        }

        public override void Reweigh()
        {
          // Shellview.Body = new WeighingViewModel();
            Shellview.State = new WeighingState(Shellview, _user); 
        }

        public override void Reject()
        {
            Shellview.State = new StartState(Shellview);
        }
    }
}
