﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CAALHP.SOA.ICE.ClientAdapters;
using CW.Events;
using CareWeight.ViewModelEvents;
using CareWeight.ViewModels;
using Caliburn.Micro;

namespace CareWeight
{
    class AppBootstrapper : Bootstrapper<IShell>, IHandle<BaseMessage>, IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private AppAdapter _adapter;
        const string endpoint = "localhost";

        public AppBootstrapper()
        {
            _adapter = new AppAdapter(endpoint, this);

            ViewModelEventAggregator.EventAggregator.Subscribe(this);
            
        }

        // Not sure this is required
        // private Thread _thread;

        #region MEF Configuration
        //This region contains setup of MEF for caliburn micro, all code comes from snipets at caliburnmicro.codeplex.com
        //the bootstrapper takes the rootmodel that will serve as a shell for the collection on views
        private CompositionContainer container;

        protected override void Configure()
        {
            container = new CompositionContainer(
                new AggregateCatalog(AssemblySource.Instance.Select(x => new AssemblyCatalog(x)))
                );

            var batch = new CompositionBatch();

            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            batch.AddExportedValue(container);

            container.Compose(batch);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            string contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            var exports = container.GetExportedValues<object>(contract);

            if (exports.Count() > 0)
                return exports.First();

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", contract));
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return container.GetExportedValues<object>(AttributedModelServices.GetContractName(serviceType));
        }

        protected override void BuildUp(object instance)
        {
            container.SatisfyImportsOnce(instance);
        }
        #endregion //End MEF Config

        #region AppCAALHP Contract/Events


        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            // Debugger.Launch();

            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(WeightInitEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ConnectionErrorEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TareErrorResponsEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TareResponsEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserOffWeightEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserOnWeightEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(WeightDataReadyEvent), "CW.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FacialErrorEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FacialAuthFailedResponsEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);

            ShowThisApp();
            
        }

        private void ShowThisApp()
        {
            var showAppEvent = new ShowAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = GetName()
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, showAppEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        public string GetName()
        {
            return "CareWeight";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Application.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Show()
        {
            // Debugger.Launch();
            // MessageBox.Show("Bootstrapper show");
            ViewModelEventAggregator.EventAggregator.Publish(new ViewModelMessage { MessageType = "Show" });
            //Application.Current.MainWindow.WindowState = WindowState.Minimized;
            //Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        //[DllImport("user32.dll")]
        //static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //public void Show()
        //{
        //    //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
        //    //Threading is used so we do not block the main thread.
        //    DispatchToApp(() =>
        //    {
        //        var proc = Process.GetProcessById(_processId);
        //        var handle = proc.MainWindowHandle;
        //        ShowWindow(handle, 3);
        //        _main.AppList = GetListOfInstalledApps();
        //        _main.Show();
        //    });
        //}

        private void HandleCaalphEvent(ShowAppEvent e)
        {
            if (e.AppName.Contains(GetName()))
            {
                //Show homescreen
                Show();
            }
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleCaalphEvent(obj);
        }

        private void HandleCaalphEvent(ConnectionErrorEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_NoConnectionError });
        }

        private void HandleCaalphEvent(TareErrorResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_Der_skete_en_fejl_under_nulstilling_af_vægten_ });
        }

        private void HandleCaalphEvent(TareResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new TareCompleteMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(UserOffWeightEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new UserOffWeightMessage { Sender = GetType() });

            LogOutUserRequest();
        }



        private void HandleCaalphEvent(UserOnWeightEvent o)
        {
            Show();
            ViewModelEventAggregator.EventAggregator.Publish(new UserOnWeightMessage { Sender = GetType() });
            //throw new NotImplementedException();
        }

        private void HandleCaalphEvent(WeightDataReadyEvent o)
        {
            var value = o.Value;
            ViewModelEventAggregator.EventAggregator.Publish(new WeightDataReadyMessage { Sender = GetType(), Value = value });
        }

        private void HandleCaalphEvent(UserLoggedInEvent o)
        {
            var userLoggedIn = o.User;
            //TODO skal vi bruge brugernavn til noget.
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationDoneMessage { Sender = GetType(), User = userLoggedIn });
        }

        private void HandleCaalphEvent(FacialAuthFailedResponsEvent o)
        {
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationFailedMessage { Sender = GetType() });
        }

        private void HandleCaalphEvent(FacialErrorEvent o)
        {
            var message = o.Message;
            ViewModelEventAggregator.EventAggregator.Publish(new IdentificationFailedMessage { Sender = GetType() });
            ViewModelEventAggregator.EventAggregator.Publish(new StatusMessage { Status = Resource.Resource.AppBootstrapper_HandleCaalphEvent_IdentificationFailed + message });
        }

        private void LogOutUserRequest()
        {
            var command = new UserLoggedOutEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        #endregion

        #region IHandle message

        private void HandleMessage(WeightMessage message)
        {
            var weightMeasurement = message.Value;
            string userId = message.User != null ? message.User.UserId : null;

            var weightEvent = new SimpleMeasurementEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    MeasurementType = "Weight",
                    Value = weightMeasurement,
                    UserId = userId
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, weightEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(IdentificationRequestMessage message)
        {
            var idRequest = new FacialAuthRequestEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, idRequest);
            _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(TareRequestMessage message)
        {
            var tareRequest = new TareRequestEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, tareRequest, "CW.Events");
            _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(ReweighMessage message)
        {
            var tareRequest = new WeighingRequestEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId
                };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, tareRequest, "CW.Events");
            _host.Host.ReportEvent(serializedEvent);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is ment to be empty
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
            //MessageBox.Show("BaseMessage handler called.");
        }

        #endregion

    }

}
