﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaceIdentificationAdaptor;

namespace CreateNewFacialTemplate
{
    class Program
    {
        static void Main(string[] args)
        {
            string key = "";

            Console.WriteLine("Press E to enroll new person");
            Console.WriteLine("Press anykey to start identifying");
            key = Console.ReadKey().Key.ToString();

            string id = "peter";
            if (!FaceIdentificationFacade.Instance.ModuleLoaded)
            {
                Console.WriteLine("Module failed loading");
                Console.WriteLine("Press anykey to exit");
                Console.ReadKey();
                return;
            }

            if (key.ToLower() == "e")
            {
                bool idres = false;
                try
                {
                    idres = FaceIdentificationFacade.Instance.EnrollNewPerson(id);
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed creating template");
                    Console.WriteLine("Press anykey to exit");
                    Console.ReadKey();
                    return;
                }

                if (!idres)
                {
                    Console.WriteLine("Failed creating template");
                    Console.WriteLine("Press anykey to exit");
                    Console.ReadKey();
                    return;
                }

                Console.WriteLine("Press anykey to start identification");
                Console.ReadKey();
            }


            string result = "";
            try
            {
                result = FaceIdentificationFacade.Instance.Identify();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                Console.WriteLine("Press anykey to continue");
                Console.ReadKey();
            }

            if(id == result)
                Console.WriteLine("We have a match");
            else
            {
                Console.WriteLine("Everything is wrong");
            }


            Console.WriteLine("Press anykey to exit");
            Console.ReadKey();

            return;
        }
    }
}
