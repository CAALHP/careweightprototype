﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FI.Events
{
    public class EnrollUserErrorResponsEvent: Event
    {
        public string Error { get; set; }
        public string UserId { get; set; }
    }
}
