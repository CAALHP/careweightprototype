﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FaceIdentificationAdaptor;

namespace FaceidentificationConsoleDistancetest
{
    class Program
    {
        static int Main(string[] args)
        {

            Stopwatch stopwatch = new Stopwatch();
            Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            long totalBytesOfMemoryUsed;



            Console.WriteLine("Trying to enroll from webcam");
            Console.WriteLine("Press anykye to continue");
            Console.Read();

            string id = "peter";
            if (!FaceIdentificationFacade.Instance.ModuleLoaded)
            {
                Console.WriteLine("Module failed loading");
                return -1;
            }


            Byte[] template;

            template = FaceIdentificationFacade.Instance.EnrollNewPerson();

            if (template != null)
                FaceIdentificationFacade.Instance.AddEntry(id, template);
            else
            {
                Console.WriteLine("Failed creating template");
                Console.WriteLine("Press anykey to exit");
                Console.Read();
                return -1;
            }

            Console.WriteLine("Press anykey to start continious identification.");
            Console.Read();

            int count = 0;
            int succes = 0;

            while (true)
            {
                count++;
                
                Console.WriteLine("Next Id will start in 2 seconds");

                Thread.Sleep(2000);

                var result = FaceIdentificationFacade.Instance.Identify();

                if (!string.IsNullOrEmpty(result))
                {
                    succes++;
                    Console.WriteLine("Id attempt number {0}, numbers of succes {1}", count,succes);
                }
                    
                if (count > 10)
                {
                    succes = 0;
                    count = 0;
                    Console.WriteLine("Press enter to start test again");
                    Console.Read();
                    Thread.Sleep(5000);
                }
                    

            }

        }
    }
}
