﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB4OHandler;
using DataTypeLibrary;
using Db4objects.Db4o;

namespace DB4OConsoleTest
{
    class Program
    {
        private static void Main(string[] args)
        {
            // Data to be manipulated in the database.
            // Id list
            var idList1 = new List<Identification> { new Identification(new byte[1], 1, "Facial recognition") };
            var idList2 = new List<Identification> { new Identification(new byte[1], 1, "Facial recognition") };
            
            // Person objects
            var p1 = new Person("Person1", "11111111", 170, idList1, "IHA");
            var p2 = new Person("Person2", "22222222", 180, idList1, "IHA");
            
            // Measurements objects
            var m1WithoutPerson = new Measurement("Weight", 80);
            var m2WithPerson = new Measurement("Weight", 90, p2);
            
            // to reset the database.

            // Tests
            // Add persons and meassurements
            AwtsDbFacade.Instance.NewPerson(p1);
            AwtsDbFacade.Instance.NewPerson(p2);
            AwtsDbFacade.Instance.NewMeasurement(m1WithoutPerson);
            Console.WriteLine("Adding new person and measurements to database.");
            Person getPerson1 = AwtsDbFacade.Instance.GetPerson("11111111");
            AwtsDbFacade.Instance.NewMeasurement(m1WithoutPerson);
            
            
            // AddIdentification
            Console.WriteLine("1st time");
            for (int i = 0; i < getPerson1.IdList.Count; i++)
            {
                Console.WriteLine("ID: {0}. Type: {1}", getPerson1.IdList.ElementAt(i).Id, getPerson1.IdList.ElementAt(i).Type);
            }

            getPerson1.AddIdentification(new Identification(new byte[1], 2, "New type"));

            Console.WriteLine("2nd time");
            for (int i = 0; i < getPerson1.IdList.Count; i++)
            {
                Console.WriteLine("ID: {0}. Type: {1}", getPerson1.IdList.ElementAt(i).Id, getPerson1.IdList.ElementAt(i).Type);
            }

            // Get a person
            Person getPerson2 = AwtsDbFacade.Instance.GetPerson("22222222");
            Console.WriteLine("Trying to get person with cpr 22222222.\nPerson found. Name: {0}.", getPerson2.Name);
            
            
            // Get meassurements
            List<Measurement> getMeasurement1 = AwtsDbFacade.Instance.GetMeasurements("11111111");
            Console.WriteLine("Person with cpr 11111111 has a total of {0} meassurements.", getMeasurement1.Count);
            
            for (int i = 0; i < getMeasurement1.Count; i++)
            {
                Console.WriteLine("The meassurementvalues are: {0} of type {1}. Qualitycomments: {2}", getMeasurement1.ElementAt(i).Value, getMeasurement1.ElementAt(i).Type, getMeasurement1.ElementAtOrDefault(i).Quality);
            }

            
            // Get all Person objects WITHOUT Identification 
            List<string> personList1 = AwtsDbFacade.Instance.GetAllPersonsWithoutIdentification();

            for (int i = 0; i < personList1.Count; i++)
            {
                Console.WriteLine("Get all persons without identification, Name: {0}. Person number {1}", personList1.ElementAt(i), i+1);
            }

            // Get all Person objects WITH Identification
            List<Person> personList2 = AwtsDbFacade.Instance.GetAllPersonsWithIdentification();

            for (int i = 0; i < personList2.Count; i++)
            {
                Console.WriteLine("Get all persons with identification, Name: {0}. IdentificationID: {1}, IdentificationType: {2}", personList2.ElementAt(i).Name, personList2.ElementAt(i).IdList.FirstOrDefault().Id, personList2.ElementAt(i).IdList.FirstOrDefault().Type);
            }
            
        }
    }
}
