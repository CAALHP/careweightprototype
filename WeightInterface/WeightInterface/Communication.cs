﻿using System;
using System.IO.Ports;

namespace WeightInterface
{

    public delegate void DataRecievedEvent(object sender, DataEventArgs e);

    internal class Communication
    {
        private string buffer = String.Empty;
        private int recieve_ctr = 0;
        private SerialPort com = new SerialPort();

        public event DataRecievedEvent dataRecieved;

        public Communication()
        {
            com.DataReceived += DataReceived;

            BaudRate = 9600;
            DataBits = 8;
            Parity = Parity.None;
        }

        public int BaudRate { get; set; }
        public int DataBits { get; set; }
        public Parity Parity { get; set; }

        public void Dispose()
        {
            com.DataReceived -= DataReceived;
        }

        public bool OpenPort(string comPort)
        {
            try
            {
                if (com.IsOpen) com.Close();

                com.BaudRate = BaudRate;
                com.DataBits = DataBits;
                com.Parity = Parity;
                com.PortName = comPort;

                com.Open();
                return true;
            }
            catch
            {
                com.Close();
                return false;
            }
        }

        public bool OpenPort()
        {
            if (com.IsOpen) com.Close();

            com.BaudRate = BaudRate;
            com.DataBits = DataBits;
            com.Parity = Parity;

            foreach (var port in SerialPort.GetPortNames())
            {
                try
                {
                    Console.WriteLine("Connecting to port " + port);
                    com.PortName = port;
                    com.Open();

                    Console.WriteLine("!! Connected to port :" + port);
                    return true;
                }
                catch (Exception)
                {
                    com.Close();
                }
            }

            return false;
        }

        public bool IsOpen()
        {
            return com.IsOpen;
        }

        public void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            char[] inputData = new char[1];

            while (com.BytesToRead > 0)
            {
                // Get one char at the time
                inputData[0] = (char)com.ReadChar();

                //Store data in buffer
                buffer += inputData[0];

                //Increment counter
                recieve_ctr++;
            }

            if (recieve_ctr == 5)
            {
                if (dataRecieved != null)
                {
                    if (dataRecieved != null)
                        dataRecieved(this, new DataEventArgs(buffer));
                }

                //Empty buffer
                buffer = string.Empty;
                recieve_ctr = 0;
            }
        }

        public void DataSend(string text)
        {
            if (com.IsOpen)
            {
                com.Write(text);
            }
        }

        public void Close()
        {
            if (com.IsOpen)
                com.Close();
        }
    }
}