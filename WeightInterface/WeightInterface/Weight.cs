﻿using System;
using System.Timers;

namespace WeightInterface
{
    
    public class Weight
    {
        public delegate void DataReadyEventHandler(object sender, WeightEventArgs e);
        public delegate void TareCompletedEventHandler(object sender, TareEventArgs e);
        /// <summary>
        /// Occurs when the user steps on the weight
        /// </summary>
        public event EventHandler OnUserOnWeight;
        /// <summary>
        /// Occurs when the weight data is received from the weight
        /// </summary>
        public event DataReadyEventHandler OnWeightDataReady;
        /// <summary>
        /// Occurs when the person steps off the weight
        /// </summary>
        public event EventHandler OnUserOffWeight;
        /// <summary>
        /// Occurs when the weight sends ready response
        /// </summary>
        public event EventHandler OnWeightInit;
        /// <summary>
        /// Occurs when the weight sends ready response
        /// </summary>
        public event EventHandler OnConnectionError;
        /// <summary>
        /// Occurs when the taring is completed
        /// </summary>
        public event TareCompletedEventHandler OnTareCompleted;

        private static Communication com;
        private string _comPort;
        private Timer _heartBeatTimer = new Timer(30000);
        //public bool IsOpen { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Weight class, and trys to connect to the weight automatically.
        /// </summary>
        public Weight()
        {
            com = new Communication();
            com.OpenPort();
            com.dataRecieved += ComDataRecieved;
            _heartBeatTimer.Elapsed += _heartBeatTimer_Elapsed;
            _heartBeatTimer.Start();
        }

        /// <summary>
        /// Initializes a new instance of the Weight class, and sets the COMport to the COMport chosen as parameter.
        /// </summary>
        /// <param name="comPort">The COMport that the weight is connected to. Ex. "COM7"</param>
        public Weight(string comPort)
        {
            _comPort = comPort;
            com = new Communication();
            com.OpenPort(comPort);
            com.dataRecieved += ComDataRecieved;
            _heartBeatTimer.Elapsed += _heartBeatTimer_Elapsed;
            _heartBeatTimer.Start();
        }

        public void Dispose()
        {
            _heartBeatTimer.Stop();
            _heartBeatTimer.Elapsed -= _heartBeatTimer_Elapsed;
            com.dataRecieved -= ComDataRecieved;
            com.Dispose();
            com = null;
        }

        /// <summary>
        /// Trys to open a seriel connection.
        /// </summary>
        /// <returns></returns>
        public bool TryOpen()
        {
            if (IsOpen()) return IsOpen();

           if (string.IsNullOrEmpty(_comPort))
                com.OpenPort();
            else
                com.OpenPort(_comPort);
            if (IsOpen())
            {
                Console.WriteLine("!! Connected !!");
                ManualInit();
            }
                

            

            return IsOpen();
        }

        public bool IsOpen()
        {
            return com.IsOpen();
        }

        void _heartBeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsOpen()) return;

            if (!TryOpen())
            {
                Console.WriteLine("No connection, trying to reconnect.");
                RaiseOnConnectionError();
            }
        }

        private void ComDataRecieved(object sender, DataEventArgs e)
        {
            string streng = e.Data;
            double weight = 0;

            Console.WriteLine("Recieved data from weight : " + streng);

            if (streng.StartsWith("W"))
            {
                if (streng == "WINIT")
                {
                    // If the command is WINIT throw the OnUserOnWeigh event
                    RaiseUserOnWeight();
                }
                else if (streng == "WPOFF")
                {
                    // If the command is WPOFF throw the OnUserOffWeight event
                    RaiseUserOffWeight();
                }
                else if (streng == "WT_OK")
                {
                    // If the command is WT_OK throw the OnTareCompleted event with the message OK
                    RaiseTareComplete("OK");
                }
                else if (streng == "WT_ER")
                {
                    // If the command is WT_ER throw the OnTareCompleted event with the message ERROR
                    RaiseTareComplete("ERROR");
                }
                else if (double.TryParse(streng.Substring(streng.LastIndexOf('W') + 1), out weight))
                {
                    // Calculates the weight
                    weight = weight / 10;
                    // Throw the OnWeightDataReady event with the weight as the parameter
                    RaiseWeightDataReady(weight);
                }
            }
            else if (streng == "READY")
            {
                RaiseOnWeightInit();
            }

            //resets heartbeattimer so it doesnt elapsed
            _heartBeatTimer.Stop();
            _heartBeatTimer.Start();
        }

        private void RaiseWeightDataReady(double weight)
        {
            if (OnWeightDataReady != null)
                OnWeightDataReady(this, new WeightEventArgs(weight));
        }

        private void RaiseTareComplete(string msg)
        {
            if (OnTareCompleted != null)
                OnTareCompleted(this, new TareEventArgs(msg));
        }

        private void RaiseUserOffWeight()
        {
            if (OnUserOffWeight != null)
                OnUserOffWeight(this, new EventArgs());
        }

        private void RaiseUserOnWeight()
        {
            if (OnUserOnWeight != null)
                OnUserOnWeight(this, new EventArgs());
        }

        protected void RaiseOnWeightInit()
        {
            var handler = OnWeightInit;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected void RaiseOnConnectionError()
        {
            var handler = OnConnectionError;
            if (handler != null) handler(this, EventArgs.Empty);
        }


        /// <summary>
        /// Sends the reweighing command to the weight
        /// </summary>
        public void ManualWeighing()
        {
            // Sends the command WR
            Console.WriteLine("Requesting Manual Weighing\n");
            com.DataSend("WR");
        }

        /// <summary>
        /// Sends the manual taring command to the weight
        /// </summary>
        public void ManualTare()
        {
            // Sends the command WR
            Console.WriteLine("Requesting Manual Tare\n");
            com.DataSend("WT");
        }

        /// <summary>
        /// Send a initialize for handshake
        /// </summary>
        public void ManualInit()
        {
            // Sends the command WI
            Console.WriteLine("Requesting Manual Init\n");
            com.DataSend("WT");
        }
    }
}
