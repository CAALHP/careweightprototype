﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeightInterface
{
    public class TareEventArgs : EventArgs
    {
        private string buffer;

        public string Buffer
        {
            get
            {
                return buffer;
            }
        }

        public TareEventArgs(string buff)
        {
            buffer = buff;
        }
    }
}
