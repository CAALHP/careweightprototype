﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeightInterface
{
    public class DataEventArgs : EventArgs
    {
        private string data;
        public DataEventArgs(string Data)
        {
            data = Data;
        }

        public string Data
        {
            get { return data; }
        }
    }
}
