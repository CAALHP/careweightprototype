﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeightInterface
{
    public class WeightEventArgs : EventArgs
    {
        private double buffer;

        public double Buffer
        {
            get
            {
                return buffer;
            }
        }

        public WeightEventArgs(double buff)
        {
            buffer = buff;
        }
    }
}
